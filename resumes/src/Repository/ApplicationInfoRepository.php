<?php

namespace App\Repository;

use App\Entity\ApplicationInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ApplicationInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApplicationInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApplicationInfo[]    findAll()
 * @method ApplicationInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApplicationInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ApplicationInfo::class);
    }




    public function selectApplicantInfo($useId, $status)
    {
        return $this->createQueryBuilder('a')
            ->select('a.jobLevel, a.jobCategoryId, a.availableFor, a.jobCategoryTitle')
            ->where('a.userId = :val')
            ->setParameter('val', $useId)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;
    }


    // /**
    //  * @return ApplicationInfo[] Returns an array of ApplicationInfo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ApplicationInfo
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
