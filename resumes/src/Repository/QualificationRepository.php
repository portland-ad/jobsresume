<?php

namespace App\Repository;

use App\Entity\Qualification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Qualification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Qualification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Qualification[]    findAll()
 * @method Qualification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QualificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Qualification::class);
    }


    public function selectQualificationBasic($userId, $status)
    {
        $data =  $this->createQueryBuilder('b')
            ->select('b.id,b.subject,b.institute')
            ->leftJoin('App\Entity\ExamList', 'e','WITH', 'b.exam = e.id')
            ->addSelect('e.title exam_title')
            ->where('b.userId = :val')
            ->andWhere('b.status= :val1')
            ->setParameter('val', $userId)
            ->setParameter('val1', $status)
            ->orderBy('b.updateAt', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;
        return $data;
    }


    // /**
    //  * @return Qualification[] Returns an array of Qualification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Qualification
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
