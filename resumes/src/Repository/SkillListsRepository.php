<?php

namespace App\Repository;

use App\Entity\SkillLists;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SkillLists|null find($id, $lockMode = null, $lockVersion = null)
 * @method SkillLists|null findOneBy(array $criteria, array $orderBy = null)
 * @method SkillLists[]    findAll()
 * @method SkillLists[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillListsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SkillLists::class);
    }

    // /**
    //  * @return SkillLists[] Returns an array of SkillLists objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SkillLists
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
