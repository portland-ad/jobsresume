<?php

namespace App\Repository;

use App\Entity\JobAppliers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JobAppliers|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobAppliers|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobAppliers[]    findAll()
 * @method JobAppliers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobAppliersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JobAppliers::class);
    }

    // /**
    //  * @return JobAppliers[] Returns an array of JobAppliers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JobAppliers
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function selectJobApplierBasic($userId,$jobId)
    {
        return  $this->createQueryBuilder('b')
            ->select('b.expectedSalary  expected_salary,b.summaryView summary_view,b.detailsView details_view,b.currencyCode currency_code,b.interViewCall inter_view_call ,b.shortListed short_listed,b.createdAt created_at')
            ->where('b.userId = :val')
            ->andWhere('b.jobId= :val1')
            ->setParameter('val', $userId)
            ->setParameter('val1', $jobId)
            ->orderBy('b.createdAt', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;
    }




	public function isThisUserApply($searchResult, $userId){

    $list = Array();
		foreach($searchResult as $key=>$val){
			if(is_object($val)){
				$jobId = $val->getId();
			}
			else{
				$jobId = $val['id'];
			}
			$applied = $this->findOneBy(['jobId'=>$jobId,'userId'=>$userId]);
			if($applied){
				if(is_object($val))
					$val->setApplied(1);
				else $val['applied'] = 1 ;
			}
			array_push($list,$val);
		}
        $searchResult->setItems($list);
		return $searchResult ;
	}
	public function alreadyApplied($jobId,$userId){
		$data = $this->findOneBy(['jobId'=>$jobId,'userId'=>$userId]);
		return $data?true:false;
	}
}
