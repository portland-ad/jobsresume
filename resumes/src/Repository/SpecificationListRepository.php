<?php

namespace App\Repository;

use App\Entity\SpecificationList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SpecificationList|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpecificationList|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpecificationList[]    findAll()
 * @method SpecificationList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecificationListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpecificationList::class);
    }

    // /**
    //  * @return SpecificationList[] Returns an array of SpecificationList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SpecificationList
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
