<?php

namespace App\Repository;

use App\Entity\Biodata;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Biodata|null find($id, $lockMode = null, $lockVersion = null)
 * @method Biodata|null findOneBy(array $criteria, array $orderBy = null)
 * @method Biodata[]    findAll()
 * @method Biodata[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BiodataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Biodata::class);
    }


    public function selectBioDataBasic($userId, $status)
    {
       return  $this->createQueryBuilder('b')
            ->select('b.id,b.userId,b.fullName full_name,b.photo,b.address,b.city,b.dateOfBirth date_of_birth, (SUBSTRING(CURRENT_DATE(),1,4) - SUBSTRING(b.dateOfBirth,1,4)) AS age')
            ->where('b.userId = :val')
            ->andWhere('b.status= :val1')
            ->setParameter('val', $userId)
            ->setParameter('val1', $status)
            ->orderBy('b.updateAt', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Biodata[] Returns an array of Biodata objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findSpecificDataByUser($user)
    {
        return $this->createQueryBuilder('b')
            ->select('b.fullName, b.photo')
            ->andWhere('b.userId = :val')
            ->setParameter('val', $user)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }


    public function calculateTotalExperiences($userId){
        $sql = '
SELECT
      CASE WHEN SUM(d.months) > 12 THEN SUM(d.years) + FLOOR(SUM(d.months/12)) ELSE SUM(d.years) END
      AS years
    , CASE WHEN SUM(d.days) > 30 THEN FLOOR(MOD((SUM(d.months) + FLOOR(SUM(d.days/30.4375))),12)) ELSE FLOOR(MOD(SUM(d.months), 12)) END
      AS months
    , CASE WHEN SUM(d.days) > 30 THEN FLOOR(MOD(SUM(d.days), 30)) ELSE FLOOR(MOD(SUM(d.days), 30)) END
      AS days
   
FROM (
      SELECT
              TIMESTAMPDIFF(YEAR , from_date, to_date )
              AS years
            , TIMESTAMPDIFF(MONTH, from_date + INTERVAL TIMESTAMPDIFF(YEAR , from_date, to_date) YEAR  , to_date )
              AS months
            , TIMESTAMPDIFF(DAY  , from_date + INTERVAL TIMESTAMPDIFF(MONTH, from_date, to_date) MONTH , to_date )
              AS days
        FROM (
            SELECT
                 from_date
                , COALESCE(to_date,CURRENT_DATE) to_date
            FROM experiences WHERE `user_id` ="'.$userId.'" AND `status` = 1 
          ) d2
    ) d
';
        //echo $sql;die();
        return $this->getEntityManager()->getConnection()->executeQuery($sql)->fetch();
    }

    public function deleteDataByUser($userId, $table){
        $sql = 'Delete From '.$table.' Where `user_id` ="'.$userId.'"';
        //echo $sql;die();
        return $this->getEntityManager()->getConnection()->executeQuery($sql);
    }

    public function resumeList($data){

        $sql = '
SELECT b.`full_name`, dts.years, dts.months, dts.days
FROM `biodata` AS b
LEFT JOIN
(

SELECT
      CASE WHEN SUM(d.months) > 12 THEN SUM(d.years) + FLOOR(SUM(d.months/12)) ELSE SUM(d.years) END
      AS years
    , CASE WHEN SUM(d.days) > 30 THEN FLOOR(MOD((SUM(d.months) + FLOOR(SUM(d.days/30.4375))),12)) ELSE FLOOR(MOD(SUM(d.months), 12)) END
      AS months
    , CASE WHEN SUM(d.days) > 30 THEN FLOOR(MOD(SUM(d.days), 30)) ELSE FLOOR(MOD(SUM(d.days), 30)) END
      AS days,
      user_id AS exp_user_id
   
FROM (
      SELECT
              TIMESTAMPDIFF(YEAR , from_date, to_date )
              AS years
            , TIMESTAMPDIFF(MONTH, from_date + INTERVAL TIMESTAMPDIFF(YEAR , from_date, to_date) YEAR  , to_date )
              AS months
            , TIMESTAMPDIFF(DAY  , from_date + INTERVAL TIMESTAMPDIFF(MONTH, from_date, to_date) MONTH , to_date )
              AS days,
              d2.user_id AS user_id
        FROM (
            SELECT
                 from_date,user_id
                , COALESCE(to_date,CURRENT_DATE) to_date
            FROM experiences WHERE `user_id` ="c3016fdc-9d11-11ea-8d83-0695fa2a8f22" AND `status` = 1 
          ) d2
    ) d) AS dts ON dts.exp_user_id = b.`user_id`
    WHERE b.`user_id` = \'c3016fdc-9d11-11ea-8d83-0695fa2a8f22\' AND  dts.years BETWEEN \'4\' AND \'7\'';
        //echo $sql;die();
        return $this->getEntityManager()->getConnection()->executeQuery($sql)->fetch();
    }


}
