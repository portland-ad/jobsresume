<?php

namespace App\Repository;

use App\Entity\ResumeStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ResumeStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResumeStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResumeStatus[]    findAll()
 * @method ResumeStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResumeStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResumeStatus::class);
    }

    // /**
    //  * @return ResumeStatus[] Returns an array of ResumeStatus objects
    //  */
   
    public function findByResumeStatus($value)
    {
        return $this->createQueryBuilder('r')
            ->Where('r.userId = :val')
            ->andWhere('r.status <> :not_status')
            ->setParameters([
                'val'   => $value,
                'not_status' => 0
                ])
            ->getQuery()
            ->getOneOrNullResult() ;
    }
    

    /*
    public function findOneBySomeField($value): ?ResumeStatus
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
