<?php

namespace App\Repository;

use App\Entity\MilitaryServices;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MilitaryServices|null find($id, $lockMode = null, $lockVersion = null)
 * @method MilitaryServices|null findOneBy(array $criteria, array $orderBy = null)
 * @method MilitaryServices[]    findAll()
 * @method MilitaryServices[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MilitaryServicesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MilitaryServices::class);
    }

    // /**
    //  * @return MilitaryServices[] Returns an array of MilitaryServices objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MilitaryServices
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
