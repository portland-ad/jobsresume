<?php

namespace App\Repository;

use App\Entity\Skills;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Skills|null find($id, $lockMode = null, $lockVersion = null)
 * @method Skills|null findOneBy(array $criteria, array $orderBy = null)
 * @method Skills[]    findAll()
 * @method Skills[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Skills::class);
    }

    // /**
    //  * @return Skills[] Returns an array of Skills objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Skills
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    public function selectSkillBasic($userId)
    {

         return $this->createQueryBuilder('b')
            ->select('b.skillId,b.skillTitle')
            ->where('b.userId= :val')
            ->setParameter('val',$userId)
            ->getQuery()
            ->getResult()
          ;

    }


    public function getSkillListByUser($userId){
        $sql='SELECT  GROUP_CONCAT(skl.`title` ORDER BY skl.`title` SEPARATOR \', \' ) `name`, GROUP_CONCAT(skl.`id` ORDER BY skl.`title` SEPARATOR \', \' ) `id`
FROM `skill_lists` skl INNER JOIN `skills` sk
  ON CONCAT(\',\',sk.`skill_id`,\',\') LIKE CONCAT(\'%,\',skl.id,\',%\')
  WHERE sk.`user_id`="'.$userId.'"
GROUP BY sk.`skill_id`;';

        return $this->getEntityManager()->getConnection()->executeQuery($sql)->fetch();
    }
}
