<?php


namespace App\Services;


use App\Entity\JobAppliers;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerBuilder;
use stdClass;
use Symfony\Component\HttpClient\HttpClient;

class ResumeService
{
    const GATEWAY_URL = 'http://13.58.205.236:8080';
    const ADMIN_SERVICE_KEY = '';
    private $em;
    private $serializer;
    private $httpClient;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->serializer = SerializerBuilder::create()->build();
        $this->httpClient = HttpClient::create();
    }

    public function getQuery($data, $jobId, $shortList):array
    {


        $query = '';
        $fromFlag = false;
        $filterFlag = false;
        $isApplierInfoFlag = false;
        $existingSkillFlag = false;
        $existingBiodata = $this->em->getRepository('App:Biodata')->findOneBy(['status'=>1]);
        $existingQualification = $this->em->getRepository('App:Qualification')->findBy([ 'status'=>1]);
        $existingExperiences = $this->em->getRepository('App:Experiences')->findBy([ 'status'=>1]);
        $existingApplicationInfo = $this->em->getRepository('App:ApplicationInfo')->findOneBy([ 'status'=>1]);

        $existingSkills = $this->em->getRepository('App:Skills')->findAll();
        $applierInfo = $this->em->getRepository(JobAppliers::class)->findOneBy(['jobId'=>$jobId]);
        if(!$applierInfo) return [];

        if($existingBiodata) {
            //bio data must be existed that why it set as from
            if(!$fromFlag) {
                if($existingExperiences)
                {
                    $query = $query ." select bd.user_id, dts.years,dts.months,dts.days from biodata bd";
                }
                else
                {
                    $query = $query ." select bd.user_id from biodata bd";
                }

                $fromFlag = true;
            }
        }

        if($existingQualification) {
            $query = $query ." left join qualification qlftn on bd.user_id = qlftn.user_id";
            $query = $query ." left join exam_list ex_li on qlftn.exam_id = ex_li.id";
        }

        if($existingExperiences) {
            $query = $query ." left join experiences ex on bd.user_id = ex.user_id ";
            $query = $query . "LEFT JOIN".
                            " (".

                                " SELECT".
                                     " CASE WHEN SUM(d.months) > 12 THEN SUM(d.years) + FLOOR(SUM(d.months/12)) ELSE SUM(d.years) END ".
                                      " AS years ".
                                    " , CASE WHEN SUM(d.days) > 30 THEN FLOOR(MOD((SUM(d.months) + FLOOR(SUM(d.days/30.4375))),12)) ELSE FLOOR(MOD(SUM(d.months), 12)) END ".
                                     " AS months".
                                    " , CASE WHEN SUM(d.days) > 30 THEN FLOOR(MOD(SUM(d.days), 30)) ELSE FLOOR(MOD(SUM(d.days), 30)) END ".
                                     " AS days,".
                                      " user_id AS exp_user_id ".

                                       "  FROM (  ".
                                       "     SELECT ".
                                       "            TIMESTAMPDIFF(YEAR , from_date, CASE WHEN to_date <> '' THEN to_date ELSE CURRENT_DATE END ) ".
                                       "           AS years ".
                                       "           , TIMESTAMPDIFF(MONTH, from_date + INTERVAL TIMESTAMPDIFF(YEAR , from_date, CASE WHEN to_date <> '' THEN to_date ELSE CURRENT_DATE END) YEAR  , CASE WHEN to_date <> '' THEN to_date ELSE CURRENT_DATE END )  ".
                                       "            AS months ".
                                        "         , TIMESTAMPDIFF(DAY  , from_date + INTERVAL TIMESTAMPDIFF(MONTH, from_date, CASE WHEN to_date <> '' THEN to_date ELSE CURRENT_DATE END) MONTH , CASE WHEN to_date <> '' THEN to_date ELSE CURRENT_DATE END ) ".
                                        "          AS days, ".
                                       "         d2.user_id AS user_id ".
                                       "  FROM ( ".
                                       "     SELECT ".
                                       "         from_date,user_id ".
                                       "       , COALESCE(to_date,CURRENT_DATE) to_date ".
                                       "        FROM experiences WHERE STATUS = 1  ".
                                       "     ) d2 ".
                                       "     ) d GROUP BY d.user_id) AS dts ON dts.exp_user_id = bd.user_id";
        }

        if($existingApplicationInfo) {
            $isApplierInfoFlag = true;
            $query = $query ." left join application_info ap_in on bd.user_id = ap_in.user_id";
        }

        if($existingSkills) {
            $existingSkillFlag = true;
            $query = $query ." left join skills sk on bd.user_id = sk.user_id ";
        }


        if($applierInfo) {
            $query = $query ." inner join job_appliers j_a on bd.user_id = j_a.user_id ";
        }

        $name = $data['name'];
        $gender = $data['gender'];
        $city = $data['location'];
        $lavel = $data['job_level'];
        $qualificationFlag = (isset($data['qualification'])&&!empty($data['qualification']))?true:false;
        $salary_range_min = intval($data['salary_start_range']);
        $salary_range_max = intval($data['salary_end_range']);
        $minExperience = (isset($data['experience_start_range'])?$data['experience_start_range']:'');
        $maxExperience = (isset($data['experience_end_range'])?$data['experience_end_range']:'');

        $skill = explode(',',$data['skill_list']);


        if($name) {
            if($filterFlag) {
                $query = $query. " && bd.full_name like '%".trim($name)."%' ";
            } else
            {
                $query = $query. " where j_a.job_id = '".$jobId."' && bd.full_name like '%".trim($name)."%' ";
                $filterFlag = true;
            }
        }

        if($gender) {
            if($filterFlag) {
                $query = $query. " && bd.gender like '".trim($gender)."' ";
            } else
            {
                $query = $query. " where j_a.job_id = '".$jobId."' &&  bd.gender like '".trim($gender)."' ";
                $filterFlag = true;
            }
        }

        if($city) {
            if($filterFlag) {
                $query = $query. " && bd.city like'%".trim($city)."%' ";
            } else
            {
                $query = $query. " where j_a.job_id = '".$jobId."' && bd.city like'%".trim($city)."%' ";
                $filterFlag = true;
            }
        }

        if($shortList) {
            if($filterFlag) {
                $query = $query. " &&  j_a.short_listed  = 1 ";
            } else
            {
                $query = $query. " where j_a.job_id = '".$jobId."' && j_a.short_listed = 1 ";
                $filterFlag = true;
            }
        }



        if($isApplierInfoFlag && $lavel) {
            if($filterFlag) {
                $query = $query. " && ap_in.job_level  like '%".addslashes($lavel)."%' ";
            } else
            {
                $query = $query. " where j_a.job_id = '".$jobId."' && ap_in.job_level like '%".addslashes(trim($lavel))."%' ";
                $filterFlag = true;
            }
        }

        if($skill && $existingSkillFlag && !empty($skill[0] && $existingSkills)) {

            if($filterFlag) {
                foreach ($skill as $sk)
                {
                    $query = $query. " && sk.skill_title like '%".addslashes(trim($sk))."%' ";
                }

            } else
            {
                $skillWhereFlag = false;
                foreach ($skill as $sk)
                {
                    if(!$skillWhereFlag)
                    {

                        $skillWhereFlag = true;
                        $query = $query. " where j_a.job_id = '".$jobId."' && sk.skill_title like '%".addslashes(trim($sk))."%' ";
                        continue;
                    }
                    $query = $query. " && sk.skill_title like '%".addslashes(trim($sk))."%' ";
                }
                $filterFlag = true;
            }
        }

        if($existingQualification && $qualificationFlag)
        {
            if($filterFlag) {
                $query = $query. " && ex_li.title like'%".addslashes(trim($data['qualification']))."%' ";
            } else
            {
                $query = $query. "  where j_a.job_id = '".$jobId."' && ex_li.title like'%".addslashes(trim($data['qualification']))."%' ";
                $filterFlag = true;
            }
        }

        if($salary_range_min && $salary_range_max) {
            if($filterFlag) {
                $query = $query. " && j_a.expected_salary >=".$salary_range_min." && j_a.expected_salary<= $salary_range_max";
            } else
            {
                $query = $query. "  where j_a.job_id = '".$jobId."' && j_a.expected_salary >=".$salary_range_min." && j_a.expected_salary<= $salary_range_max";
                $filterFlag = true;
            }
        }
        else
        {
            $salary = $salary_range_min || $salary_range_max;
            if($salary) {
                if ($filterFlag) {
                    $query = $query . " && j_a.expected_salary >=" . $salary;
                } else {
                    $query = $query . "  where j_a.job_id = '".$jobId."' && j_a.expected_salary >=" . $salary;
                    $filterFlag = true;
                }
            }
        }
         if($maxExperience) {
             if($filterFlag) {
                 $query = $query. " &&  dts.years BETWEEN '".addslashes($minExperience)."' AND '".addslashes($maxExperience)."'";

             } else
             {
                 $query = $query. "  where j_a.job_id = '".$jobId."' && dts.years BETWEEN '".addslashes($minExperience)."' AND '".addslashes($maxExperience)."'";
                 $filterFlag = true;
             }
         }
         if(!$filterFlag)  $query = $query. " where j_a.job_id = '".$jobId."'";
        $query = $query . ' group by bd.user_id';


        $data = $this->em->getConnection()->executeQuery( $query)->fetchAll();
        return $data?$data:[];

    }

    public function getIndividualResume(string $candidateId, string $jobId)
    {

        $data=[];
        $em = $this->em;
        $existingBiodata = $em->getRepository('App:Biodata')->selectBioDataBasic($candidateId, 1);
        $data['biodata'] = $existingBiodata;
        $existingQualification = $em->getRepository('App:Qualification')->selectQualificationBasic($candidateId ,1);
        $data['qualification'] = $existingQualification;
//        $existingCertificates = $em->getRepository('App:Certificates')->findBy(['userId'=>$candidateId, 'status'=>1]);
//        $data['certificates'] = $existingCertificates;
//        $existingTraining = $em->getRepository('App:Training')->findBy(['userId'=>$candidateId, 'status'=>1]);
//        $data['training'] = $existingTraining;
        $existingExperiences = $em->getRepository('App:Experiences')->selectExperienceBasic($candidateId, 1);
        $data['experiences'] = $existingExperiences;
        $existingApplicationInfo = $em->getRepository('App:ApplicationInfo')->selectApplicantInfo($candidateId, 1);
//        $data['application_info'] = $existingApplicationInfo;
        $existingSkills = $em->getRepository('App:Skills')->selectSkillBasic($candidateId, 1);
//        $existingSkills = $em->getRepository('App:Skills')->getSkillListByUser($candidateId);
        $data['skills'] = $existingSkills;

        $data['job_appliers'] = $em->getRepository(JobAppliers::class)->selectJobApplierBasic($candidateId, $jobId);
//        $existingAwards = $em->getRepository('App:Awards')->findBy(['userId'=>$candidateId, 'status'=>1]);
//        $data['awards'] = $existingAwards;
//        $existingMilitaryServices = $em->getRepository('App:MilitaryServices')->findBy(['userId'=>$candidateId, 'status'=>1]);
//        $data['militaryServices'] = $existingMilitaryServices;
//        $existingAdditionalGroup = $em->getRepository('App:AdditionalGroup')->findBy(['userId'=>$candidateId, 'status'=>1]);
//        $data['additionalGroup'] = $existingAdditionalGroup;
//        $existingTemplate = $em->getRepository('App:EmployeeTemplate')->findBy(['userId'=>$candidateId, 'status'=>1]);
//        $data['userTemplate'] = $existingTemplate;
        return $data;
    }

    public function getJobDetails($id) {

        $request = $this->httpClient->request('GET', self::GATEWAY_URL.'/jobs/'.$id,[
            'headers'=>[
                'Authorization'=>'Bearer '.'',
                'Content-Type' => 'application/json',
            ]

            ]);
        try{
            if($request->getStatusCode() == 200)
            {

                return ($request->toArray()['data']['jobs']);
            }
            return null;
        } catch (\Exception $ex)
        {
            return null;
        }

    }

}