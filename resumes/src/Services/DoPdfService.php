<?php


namespace App\Services;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Filesystem\Filesystem;

class DoPdfService extends AbstractFOSRestController
{
    protected $snappy;
    public function __construct(\Knp\Snappy\Pdf $snappy)
    {
        $this->snappy = $snappy;
    }

    public function _pdfAction($userId, $data, $templateType = 1)
    {
        $filesystem = new Filesystem();
        $filesystem->mkdir('resume_pdf/'.$userId, 0777);
        if($filesystem->exists(['resume_pdf/'.$userId.'/'.$data['biodata']->getFullName().'.pdf'])){
            $filesystem->remove( 'resume_pdf/'.$userId.'/'.$data['biodata']->getFullName().'.pdf');
        }
        $pdfTemplate = 'resume/create.pdf.resume.twig';
        if($templateType == 2){
            $pdfTemplate = 'resume/create.pdf.resume.twig';
        }
        //return $data;
        $this->snappy->generateFromHtml(
            $this->renderView(
                $pdfTemplate,
                array(
                    'resume'  => $data
                )
            ),
            'resume_pdf/'.$userId.'/'.$data['biodata']->getFullName().'.pdf'
        );
    }

}