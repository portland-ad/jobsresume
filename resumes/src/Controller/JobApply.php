<?php


namespace App\Controller;


use App\Entity\JobAppliers;
use DateTime;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class JobApply extends AbstractFOSRestController
{
    /**
     * @Route("/resume/job-apply/{id<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}",methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param string $id
     * @return Response
     */
    public function postJobApplyAction(Request $request,ValidatorInterface $validator, string $id): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1) {
            $contents = json_decode($request->getContent(),true);
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId = 1;
            try{
            $JobAppliers = new JobAppliers();
            $JobAppliers->setJobId($id);
            $JobAppliers->setUserId($userId);
            $JobAppliers->setExpectedSalary($contents['expected_salary']);
            $JobAppliers->setCurrencyCode($contents['currency_code']);
            $JobAppliers->setStatus(1);
            $JobAppliers->setCreatedAt(new DateTime());
            $JobAppliers->setUpdatedAt(new DateTime());
            $em = $this->getDoctrine()->getManager();
            $errors = $validator->validate($JobAppliers);
            if(count($errors) > 0){
                return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            $em->persist($JobAppliers);
            $em->flush();
            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }


    /**
     * @Route("/resume/job/{jobId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/userid/{userId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/shortlist",methods={"PUT"})
     * @param Request $request
     * @param $jobId
     * @return Response
     */
    public function updateJobApplyByUser(Request $request, $jobId, $userId):Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1) {
            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository(JobAppliers::class);

            try{
                $jobApplier = $repo->findOneBy(['jobId'=>$jobId,'userId'=>$userId]);
                if(!$jobApplier){
                    throw new \Exception('job not existed');
                }
                if($jobApplier->getShortListed() == null || $jobApplier->getShortListed() == 0){
                    $jobApplier->setShortListed(1);
                }
                else $jobApplier->setShortListed(0);

                $em->flush();

            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_OK));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));

    }


}