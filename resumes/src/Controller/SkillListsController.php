<?php

namespace App\Controller;

use App\Entity\SkillLists;
use App\Form\SkillListsType;
use App\Repository\SkillListsRepository;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
//use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/resume/skill/lists")
 */
class SkillListsController extends AbstractFOSRestController
{
    /**
     * @Route("/", name="skill_lists_index", methods={"GET"})
     * @param Request $request
     * @param SkillListsRepository $skillListsRepository
     * @return Response
     */
    public function index(Request $request, SkillListsRepository $skillListsRepository): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            return $this->handleView($this->view($skillListsRepository->findAll()));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }
    
    /**
     * @Route("/new", name="skill_lists_new", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function new(Request $request, ValidatorInterface $validator): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
            //return $this->handleView($this->view($contents) );
           // die();
            $skillList = new SkillLists();
            $skillList->setTitle($contents["title"]);
            $skillList->setCreatedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($skillList);
            //$entityManager->flush();
            $errors = $validator->validate($skillList);
            if(count($errors) > 0){
                return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            try{
                $entityManager->flush();
            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }

    /**
     * @Route("/{id}", name="skill_lists_new", methods={"PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function editSkillList(Request $request, ValidatorInterface $validator, string $id):Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
            $entityManager = $this->getDoctrine()->getManager();
            $skillList = $entityManager->getRepository()->find($id);
            $skillList->setTitle($contents["title"]);
            $skillList->setUpdateAt(new \DateTime());

            $entityManager->flush();
            $errors = $validator->validate($skillList);
            if(count($errors) > 0){
                return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }

        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }


    /**
     * @Route("/{id}", name="skill_lists_delete", methods={"DELETE"})
     * @param Request $request
     * @param SkillLists $skillList
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete(Request $request, SkillLists $skillList, string $id): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
            $entityManager = $this->getDoctrine()->getManager();
            $skillList = $entityManager->getRepository()->find($id);
            $skillList->setTitle(0);
            $skillList->setUpdateAt(new \DateTime());
            $entityManager->flush();

            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_OK));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }
}
