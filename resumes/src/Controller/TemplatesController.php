<?php

namespace App\Controller;

use App\Entity\ExamList;
use App\Entity\RuleDetails;
use App\Entity\Templates;
use App\Form\TemplatesType;
use App\Repository\TemplatesRepository;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
//use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/resume/templates")
 */
class TemplatesController extends AbstractFOSRestController
{
    /**
     * @Route("/", name="templates_index", methods={"GET"})
     * @param Request $request
     * @param TemplatesRepository $templatesRepository
     * @return Response
     */
    public function index(Request $request, TemplatesRepository $templatesRepository)
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            return $this->handleView($this->view($templatesRepository->findAll()));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }
    
    /**
     * @Route("/new", name="templates_new", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function new(Request $request,  ValidatorInterface $validator): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//                ]);
            //return $this->handleView($this->view($contents) );
           // die();
            $template = new Templates();
            $template->setTemplateName($contents["name"]);
            $template->setTemplateType($contents["templateType"]);
            $template->setCreatedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($template);
            $errors = $validator->validate($template);
            if(count($errors) > 0){
                return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            try{
                $entityManager->flush();
            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }
    
    /**
     * @Route("/add/rule", name="rule_new", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function addNew(Request $request,  ValidatorInterface $validator): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//                ]);
            //return $this->handleView($this->view($contents) );
            // die();
            $rule = new RuleDetails();
            $rule->setRuleTitle($contents["ruleTitle"]);
            $rule->setRuleDescription($contents["ruleDescription"]);
            $rule->setCreatedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rule);
            $errors = $validator->validate($rule);
            if(count($errors) > 0){
                return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            try{
                $entityManager->flush();
            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }

    /**
     * @Route("/{id}", name="templates_show", methods={"GET"})
     * @param Templates $template
     * @return Response
     */
    public function show(Templates $template): Response
    {
        return $this->render('templates/show.html.twig', [
            'template' => $template,
        ]);
    }
    
    /**
     * @Route("/{id}/edit", name="templates_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Templates $template
     * @return Response
     */
    public function edit(Request $request, Templates $template): Response
    {
        $form = $this->createForm(TemplatesType::class, $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('templates_index');
        }

        return $this->render('templates/edit.html.twig', [
            'template' => $template,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/{id}", name="templates_delete", methods={"DELETE"})
     * @param Request $request
     * @param Templates $template
     * @return Response
     */
    public function delete(Request $request, Templates $template): Response
    {
        if ($this->isCsrfTokenValid('delete'.$template->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($template);
            $entityManager->flush();
        }

        return $this->redirectToRoute('templates_index');
    }


    /**
     * @Route("/{id}/delete", name="resume_templates_delete", methods={"DELETE"})
     * @param Request $request
     * @param string $id
     * @return Response
     * @throws Exception
     */
    public function delete1(Request $request, string $id): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){

            $entityManager = $this->getDoctrine()->getManager();
            $template = $entityManager->getRepository(Templates::class)->find($id);
            $template->setStatus(0);
            $template->setUpdateAt(new \DateTime());
            $entityManager->flush();

            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_OK));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));

    }
}
