<?php

namespace App\Controller;

use App\Controller\traits\UpdateResumeStatus;
use App\Controller\traits\userToken;
use App\Entity\AdditionalGroup;
use App\Entity\ApplicationInfo;
use App\Entity\Awards;
use App\Entity\Biodata;
use App\Entity\Certificates;
use App\Entity\EmployeeTemplate;
use App\Entity\Experiences;
use App\Entity\MilitaryServices;
use App\Entity\Qualification;
use App\Entity\ResumeStatus;
use App\Entity\Skills;
use App\Entity\Templates;
use App\Entity\Training;
use App\Form\TemplatesType;
use App\Services\DoPdfService;
use App\Services\ResumeService;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
//use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/resume")
 */
class ResumeController extends AbstractFOSRestController
{
    use UpdateResumeStatus, userToken;
    CONST TEMPLATE_TYPE = 1;
    //const GATEWAY_URL = 'http://localhost:8080';
    //const GATEWAY_ADMIN_URL = 'http://localhost:9876';
    const GATEWAY_URL = 'http://13.58.205.236:8080';
    const GATEWAY_ADMIN_URL = 'http://13.58.205.236:9876';
    const GATEWAY_ADMIN_API_KEY = '';
    const EMAIL_SERVICE_API_KEY = '';

    /**
     * @Route("/", name="resume_index")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        //$pp = $this->generateUrl('resume_index',[],UrlGeneratorInterface::ABSOLUTE_URL);
        //return $this->json(['status'=>$pp]);
        //echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId= 1;

            $returnTemplateTypeData  = self::_decision($userId);
            //getTemplateIDreturn $this->handleView($this->view(['status'=>'error', 'message'=>$returnTemplateTypeData],Response::HTTP_INTERNAL_SERVER_ERROR));
            if($returnTemplateTypeData['error']!=''){
                return $this->handleView($this->view(['status'=>'error', 'message'=>$returnTemplateTypeData['error']],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            // templateType =1 =default , 2 = silver type , 3= gold type, 4 = platinum
            if($returnTemplateTypeData['templateType'] == '2' && $returnTemplateTypeData['subscriptionId']!=""){
                return $this->redirectToRoute('resume_silver_index', ['userId'=>$userId]);
            }
           // return $this->handleView($this->view(['status'=>'ok','data'=>'test'],Response::HTTP_ACCEPTED));
            $data=[];
            $existingBiodata = $em->getRepository('App:Biodata')->findOneBy(['userId'=>$userId, 'status'=>1]);
            $data['biodata'] = $existingBiodata;
            $existingQualification = $em->getRepository('App:Qualification')->findBy(['userId'=>$userId, 'status'=>1],['startYear'=>'DESC']);
            $data['qualification'] = $existingQualification;
            $existingExperiences = $em->getRepository('App:Experiences')->findBy(['userId'=>$userId, 'status'=>1],['fromDate'=>'DESC']);
            $data['experiences'] = $existingExperiences;
            $existingCertificates = $em->getRepository('App:Certificates')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['certificates'] = $existingCertificates;
            $existingAwards = $em->getRepository('App:Awards')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['awards'] = $existingAwards;
            $existingAdditionalGroup = $em->getRepository('App:AdditionalGroup')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['additionalGroup'] = $existingAdditionalGroup;
            $userTotalExperiences = $em->getRepository('App:Biodata')->calculateTotalExperiences($userId);
            $data['userTotalExperiences'] = $userTotalExperiences;
            $existingApplicationInfo = $em->getRepository('App:ApplicationInfo')->findOneBy(['userId'=>$userId, 'status'=>1]);
            $data['applicationInfo'] = $existingApplicationInfo;
            $existingSkills = $em->getRepository('App:Skills')->getSkillListByUser($userId);
            $data['skills'] = $existingSkills;
            $existingTemplate = $em->getRepository('App:EmployeeTemplate')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['userTemplate'] = $existingTemplate;
            //return $this->handleView($this->view(['status'=>'ok','data'=>$data],Response::HTTP_ACCEPTED));
            $existingExamList = $em->getRepository('App:ExamList')->findAll();
            $data['examList'] = $existingExamList;
            $existingSkillList = $em->getRepository('App:SkillLists')->findAll();
            $data['skillList'] = $existingSkillList;
            $existingCountryList = $em->getRepository('App:Countries')->findAll();
            $data['countryList'] = $existingCountryList;
            return $this->handleView($this->view(['status'=>'ok','data'=>$data],Response::HTTP_ACCEPTED));
        }
        return $this->handleView($this->view(['status'=>'error', 'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }
    /* Make decision for template type [free, paid]*/
    private function _decision($userId){
        //$templateType = 1 = Default Template, 2= Silver Type, 3= Gold Type, 4 = Platinum type
        $data = [];
        $em = $this->getDoctrine()->getManager();
        $existingEmployeeTemplate = $em->getRepository('App:EmployeeTemplate')->findOneBy(['userId'=>$userId, 'status'=>1]);
        if($existingEmployeeTemplate == null){
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>1, 'templateType'=> self::TEMPLATE_TYPE]);
            //return $template;
            if($template == null){
                $data['status'] ='';
                $data['templateType'] ='';
                $data['subscriptionId'] ='';
                $data['error'] ='Template Data not found';
                return $data;
            }
            //return $template;
            try{
                $employeeTemplate = new EmployeeTemplate();
                $employeeTemplate->setUserId($userId);
                $employeeTemplate->setTemplate($template);
                $employeeTemplate->setCreatedAt(new \DateTime());
                $em->persist($employeeTemplate);
                $em->flush();
                $data['status'] ='ok';
                $data['templateType'] ='1';
                $data['subscriptionId'] ='';
                $data['error'] ='';
                return $data;
            }catch (\Exception $e){
                $data['status'] ='';
                $data['templateType'] ='';
                $data['subscriptionId'] ='';
                $data['error'] =$e;
                return $data;
            }
        }else{
            if($existingEmployeeTemplate->getTemplate()==null){
                $data['status'] ='';
                $data['templateType'] ='';
                $data['subscriptionId'] ='';
                $data['error'] ='User Template Not defined';
                return $data;
            }
            $templateType = $existingEmployeeTemplate->getTemplate()->getTemplateType();
            $data['status'] ='ok';
            $data['templateType'] =$templateType;
            $data['subscriptionId'] =$existingEmployeeTemplate->getSubscriptionId();
            $data['error'] ='';
            return $data;
        }
    }

    /**
     * @Route("/{candidateId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}", name="individual_resume")
     * @param Request $request
     * @param string $candidateId
     * @return Response
     */
    public function individualResume(Request $request, string $candidateId): Response
    {
        //return $this->json(['status'=>'Ok']);
        //echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            $jobId = $request->query->get('job_id',null);
            //$getShortlistData = $em->getRepository('App:JobAppliers')->findOneBy(['userId'=>$candidateId, 'jobId'=>$jobId, 'shortListed'=>1]);
            //$data['shortListed'] = $getShortlistData;
            //$userId= 1;
            //return $this->handleView($this->view(['status'=>'ok','job_id'=>$jobId],Response::HTTP_ACCEPTED));
            $data=[];
            if($jobId){
                $getShortlistData = $em->getRepository('App:JobAppliers')->findOneBy(['userId'=>$candidateId, 'jobId'=>$jobId, 'shortListed'=>1]);
                if($getShortlistData!=""){
                    $data['short_listed'] = 1;
                }
            }
            $existingBiodata = $em->getRepository('App:Biodata')->findOneBy(['userId'=>$candidateId, 'status'=>1]);
            $data['biodata'] = $existingBiodata;
            $existingQualification = $em->getRepository('App:Qualification')->findBy(['userId'=>$candidateId, 'status'=>1],['startYear'=>'DESC']);
            $data['qualification'] = $existingQualification;
            $existingCertificates = $em->getRepository('App:Certificates')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['certificates'] = $existingCertificates;
            $existingTraining = $em->getRepository('App:Training')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['training'] = $existingTraining;
            $existingExperiences = $em->getRepository('App:Experiences')->findBy(['userId'=>$candidateId, 'status'=>1],['fromDate'=>'DESC']);
            $data['experiences'] = $existingExperiences;
            $userTotalExperiences = $em->getRepository('App:Biodata')->calculateTotalExperiences($candidateId);
            $data['userTotalExperiences'] = $userTotalExperiences;
            $existingApplicationInfo = $em->getRepository('App:ApplicationInfo')->findOneBy(['userId'=>$candidateId, 'status'=>1]);
            $data['applicationInfo'] = $existingApplicationInfo;
            //$existingSkills = $em->getRepository('App:Skills')->findBy(['UserId'=>$candidateId, 'status'=>1]);
            $existingSkills = $em->getRepository('App:Skills')->getSkillListByUser($candidateId);
            $data['skills'] = $existingSkills;
            $existingAwards = $em->getRepository('App:Awards')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['awards'] = $existingAwards;
            $existingAdditionalGroup = $em->getRepository('App:AdditionalGroup')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['additionalGroup'] = $existingAdditionalGroup;
            $existingTemplate = $em->getRepository('App:EmployeeTemplate')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['userTemplate'] = $existingTemplate;
            return $this->handleView($this->view(['status'=>'ok','data'=>$data],Response::HTTP_ACCEPTED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/{username<^\s*([0-9a-zA-Z-.]+)\s*$>}/profile", name="individual_resume_by_user_name")
     * @param Request $request
     * @param string $username
     * @return Response
     */
    public function individualResumeByUserName(Request $request, string $username): Response
    {
        //return $this->json(['status'=>'Ok']);
        //echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId= 1;
            $candidateData = self::__getUserIdByUserName($request, $username);

            //$candidateId = 'c3016fdc-9d11-11ea-8d83-0695fa2a8f22';
            if($candidateData==""){
                return $this->handleView($this->view(['status'=>'error' ,'message'=>'User Not Found'],Response::HTTP_NOT_FOUND));
            }
            $candidateId = $candidateData['id'];
            $candidateRole = $candidateData['roles'];
            if(strtolower($candidateRole)=='company'){
                $companyData =  self::__getCompanyDataByUserId($request, $candidateId);
                if($companyData==""){
                    return $this->handleView($this->view(['status'=>'error' ,'message'=>'Company Profile Found'],Response::HTTP_NOT_FOUND));
                }
                return $this->handleView($this->view(['status'=>'ok','data'=>$companyData],Response::HTTP_OK));
            }
            $data=[];
            $data['profileType'] = 'USER';
            $existingBiodata = $em->getRepository('App:Biodata')->findSpecificDataByUser($candidateId);
            $data['biodata'] = $existingBiodata;
            $existingQualification = $em->getRepository('App:Qualification')->findBy(['userId'=>$candidateId, 'status'=>1],['startYear'=>'DESC']);
            $data['qualification'] = $existingQualification;
            $existingExperiences = $em->getRepository('App:Experiences')->findBy(['userId'=>$candidateId, 'status'=>1],['fromDate'=>'DESC']);
            $data['experiences'] = $existingExperiences;
            //$existingSkills = $em->getRepository('App:Skills')->findBy(['UserId'=>$candidateId, 'status'=>1]);
            $existingSkills = $em->getRepository('App:Skills')->getSkillListByUser($candidateId);
            $data['skills'] = $existingSkills;
            return $this->handleView($this->view(['status'=>'ok','data'=>$data],Response::HTTP_ACCEPTED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /* Get User Id By User Name*/
    public function __getCompanyDataByUserId(Request $request,  string $userId)
    {
        $tokens = null ;
        $client = HttpClient::create();

        $consumerId = $request->headers->get('ja-consumer-id',false);
        $subscriptionId = $request->headers->get('ja-user-subscription-id',false);
        /*
          Get Access Token if consumerId found
        */
        $tokens = $this->__getUserToken(self::GATEWAY_ADMIN_URL, self::GATEWAY_ADMIN_API_KEY, $consumerId);
        if($tokens==""){
            return $this->handleView($this->view(['status'=>'error' ,'message'=>'User Token miss match'],Response::HTTP_NOT_FOUND));
        }
        $userData = $client->request('GET',self::GATEWAY_URL.'/companies/'.$userId.'/public-profile',[
            'headers'=>[
                'Authorization'=>'Bearer '.is_null($tokens)?null:$tokens['access_token']
            ]
        ]);
        $userDataStatus = $userData->getStatusCode();
        if($userDataStatus == 200){
            return $userDataStatus;
        }
        return '' ;
    }

    /* Get User Id By User Name*/
    public function __getUserIdByUserName(Request $request,  string $username)
    {
        $tokens = null ;
        $client = HttpClient::create();

        $consumerId = $request->headers->get('ja-consumer-id',false);
        $subscriptionId = $request->headers->get('ja-user-subscription-id',false);
        /*
          Get Access Token if consumerId found
        */
        $tokens = $this->__getUserToken(self::GATEWAY_ADMIN_URL, self::GATEWAY_ADMIN_API_KEY, $consumerId);
        if($tokens==""){
            return $this->handleView($this->view(['status'=>'error' ,'message'=>'User Token miss match'],Response::HTTP_NOT_FOUND));
        }
        $userData = $client->request('GET',self::GATEWAY_URL.'/users/'.$username.'/id',[
            'headers'=>[
                'Authorization'=>'Bearer '.is_null($tokens)?null:$tokens['access_token']
            ]
        ]);
        $userDataStatus = $userData->getStatusCode();
        if($userDataStatus == 200){
            $temp = $userData->toArray();
            return $temp;
        }
        return '' ;
    }

    /**
     * @Route("/subscribe", name="resume_subscribe_data", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function resumeSubscribeData(Request $request,  ValidatorInterface $validator): Response
    {
        //return $this->json(['status'=>'Ok']);
        // echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
//            return $this->handleView($this->view($contents) );
//            die();
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //ja-user-subcription-id
            $subscriptionId = $request->headers->get('ja-user-subcription-id');
           //$userId = 1;
            $existingEmployeeTemplate = $em->getRepository('App:EmployeeTemplate')->findOneBy(['userId'=>$userId, 'status'=>1]);
            $templateId = self::__getTemplateIdBySubscriptionId($request);
            if($templateId==''){
                $template = $em->getRepository('App:Templates')->findOneBy(['templateType'=> self::TEMPLATE_TYPE ]);
            }else{
                $template = $em->getRepository('App:Templates')->findOneBy(['id'=> $templateId ]);
            }
            try{
                if($existingEmployeeTemplate == null){
                    $employeeTemplate = new EmployeeTemplate();
                    $employeeTemplate->setUserId($userId);
                    $employeeTemplate->setTemplate($template);
                    $employeeTemplate->setSubscriptionId($subscriptionId);
                    $employeeTemplate->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($employeeTemplate);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($employeeTemplate);
                }else{
                    $existingEmployeeTemplate->setTemplate($template);
                    $existingEmployeeTemplate->setSubscriptionId($subscriptionId);
                    $errors = $validator->validate($existingEmployeeTemplate);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $existingEmployeeTemplate->setUpdateAt(new \DateTime());
                }
                $em->flush();
            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/{candidateId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/unsubscribe", name="resume_unsubscribe_data", methods={"PUT"})
     * @param Request $request
     * @param string $candidateId
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function resumeUnSubscribeData(Request $request, string $candidateId, ValidatorInterface $validator): Response
    {
        //return $this->json(['status'=>'Ok']);
        // echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
//            return $this->handleView($this->view($contents) );
//            die();
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //ja-user-subcription-id

            //$userId = 1;
            $existingEmployeeTemplate = $em->getRepository('App:EmployeeTemplate')->findOneBy(['userId'=>$candidateId, 'status'=>1]);
            $subscriptionId = "";
            try{
                if($existingEmployeeTemplate == null){
                    return $this->handleView($this->view(['status'=>'error','message'=>'Data Not Found'],Response::HTTP_NOT_FOUND));
                }else{
                    $existingEmployeeTemplate->setSubscriptionId($subscriptionId);
                    $errors = $validator->validate($existingEmployeeTemplate);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $existingEmployeeTemplate->setUpdateAt(new \DateTime());
                }
                $em->flush();
            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_NO_CONTENT));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    public function __getTemplateIdBySubscriptionId(Request $request):string
    {
        $tokens = null ;
        $client = HttpClient::create();

        $consumerId = $request->headers->get('ja-consumer-id',false);
        $subscriptionId = $request->headers->get('ja-user-subscription-id',false);
        /*
          Get Access Token if consumerId found
        */
        $tokens = $this->__getUserToken(self::GATEWAY_ADMIN_URL, self::GATEWAY_ADMIN_API_KEY, $consumerId);
        if($tokens==""){
            return $this->handleView($this->view(['status'=>'error' ,'message'=>'User Token miss match'],Response::HTTP_NOT_FOUND));
        }
        $subscriptionData = $client->request('GET',self::GATEWAY_URL.'/subscriptions/'.$subscriptionId.'/template-id',[
            'headers'=>[
                'Authorization'=>'Bearer '.is_null($tokens)?null:$tokens['access_token']
            ]
        ]);
        $subscriptionStatus = $subscriptionData->getStatusCode();

        if($subscriptionStatus >= 400 || $subscriptionStatus >= 500){
            return '';
        }
        if($subscriptionStatus == 200){
            $temp = $subscriptionData->toArray();
            return $temp['template_id'];
        }
        return '' ;
    }

    /**
     * @Route("/status/{source}", name="resume_status", methods={"GET"})
     * @param Request $request
     * @param string $source
     * @return Response
     */
    public function resumeStatus(Request $request, string $source):Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1 ) {
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            $returnTemplateTypeData  = self::_decision($userId);
            //getTemplateIDreturn $this->handleView($this->view(['status'=>'error', 'message'=>$returnTemplateTypeData],Response::HTTP_INTERNAL_SERVER_ERROR));
            if($returnTemplateTypeData['error']!=''){
                return $this->handleView($this->view(['status'=>'error', 'message'=>$returnTemplateTypeData['error']],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            $em = $this->getDoctrine()->getManager();
            $existingResumeStatus = $em->getRepository('App:ResumeStatus')->findByResumeStatus($userId);
            // templateType =1 =default , 2 = silver type , 3= gold type, 4 = platinum
            if($returnTemplateTypeData['templateType'] == '2' && $returnTemplateTypeData['subscriptionId']!=""){
                //return $this->redirectToRoute('resume_silver_index', ['userId'=>$userId]);
                if($existingResumeStatus == null){
                    return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_NOT_FOUND));
                }
                if($existingResumeStatus->getStatus()==1){
                    return $this->handleView($this->view(['status'=>'ok','data'=>$existingResumeStatus],Response::HTTP_PARTIAL_CONTENT));
                }
                if($existingResumeStatus->getStatus()==2){
                    return $this->handleView($this->view(['status'=>'ok', 'data'=>$existingResumeStatus],Response::HTTP_PARTIAL_CONTENT));
                }
                if($existingResumeStatus->getStatus()==3){
                    return $this->handleView($this->view(['status'=>'ok', 'data'=>$existingResumeStatus],Response::HTTP_PARTIAL_CONTENT));
                }
                if($existingResumeStatus->getStatus()==4){
                    return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_OK));
                }
            }
            if($existingResumeStatus == null){
                return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_NOT_FOUND));
            }

            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_OK));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/{jobId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/applicants",  methods={"GET"})
     * @param Request $request
     * @param ResumeService $resumeService
     * @param PaginatorInterface $paginator
     * @param string $jobId
     * @return Response
     */
    public function resumeList(Request $request, ResumeService $resumeService, PaginatorInterface $paginator,string  $jobId):Response
    {
        // skill_list = php, mysql  sallary_range= 50000, 100000 name= Mr. Postman location= dubai  gender= male  job_level= expert  qualification= Masters experience= 5,7

        if(strpos($request->headers->get('content-type'),"application/json") > -1 ) {
            $page = $request->query->get('page')?$request->query->get('page'):'1';
            $limit= $request->query->get('limit')?$request->query->get('limit'):'20';
            $shortList = $request->query->get('short_list')?$request->query->get('short_list'):null;

            $em = $this->getDoctrine()->getManager();
            $contents = json_decode($request->getContent(),true);
            $userId = $request->headers->get('ja-user-id');
            $data = [];
            $data['userId'] = $userId;
            $skill_list = isset($contents['skill_list'])?$contents['skill_list']:'';
            $skill_list = $request->query->get('skill_list')?$request->query->get('skill_list'):null;

            $salary_range = isset($contents['salary_range'])?$contents['salary_range']:'';
            $salary_range = $request->query->get('salary_range')?$request->query->get('salary_range'):null;
            $experience = isset($contents['experience'])?$contents['experience']:'';
            $experience = $request->query->get('experience')?$request->query->get('experience'):null;


            $sallary_start_range = '';
            $sallary_end_range = '';
            $experience_start_range = '';
            $experience_end_range = '';

            $salary_start_range ='';
            $salary_end_range ='';
            if($salary_range!=""){
                $salary_range_array = explode("-", $salary_range);
                $salary_start_range = $salary_range_array[0];
                $salary_end_range = isset($salary_range_array[1])?$salary_range_array[1]:$salary_range_array[0];
            }
            if($experience!=""){
                $experience_range_array = explode("-", $experience);
                $experience_start_range = $experience_range_array[0];
                //$experience_end_range = $experience_range_array[1];
		$experience_end_range = isset($experience_range_array[1])?$experience_range_array[1]:$experience_range_array[0];


                /*if(!$experience_end_range) {
                    $experience_end_range = $experience_range_array[0];
                    $experience_start_range = "0";
                }*/
            }
            $name = isset($contents['name'])?$contents['name']:'';
            $name = $request->query->get('name')?$request->query->get('name'):null;
            $location = isset($contents['location'])?$contents['location']:'';
            $location = $request->query->get('location')?$request->query->get('location'):null;
            $gender =  isset($contents['gender'])?$contents['gender']:'';
            $gender = $request->query->get('gender')?$request->query->get('gender'):null;
            $job_level = isset($contents['job_level'])?$contents['job_level']:'';
            $job_level = $request->query->get('job_level')?$request->query->get('job_level'):null;
            $qualification = isset($contents['qualification'])?$contents['qualification']:'';
            $qualification = $request->query->get('qualification')?$request->query->get('qualification'):null;
            $data['skill_list'] = $skill_list;
            $data['salary_start_range'] = $salary_start_range;
            $data['salary_end_range'] = $salary_end_range;
            $data['experience_start_range'] = $experience_start_range;
            $data['experience_end_range'] = $experience_end_range;
            $data['name'] = $name;
            $data['location'] = $location;
            $data['gender'] = $gender;
            $data['job_level'] = $job_level;
            $data['qualification'] = $qualification;
            $data['job_id'] = $jobId;
            $data = $resumeService->getQuery($data,$jobId,$shortList);
            $paginateData = $paginator->paginate($data, $page, $limit);
            $dataItem = $paginateData->getItems();
            $totalCount = $paginateData->getTotalItemCount();
            $pageNumber = $paginateData->getCurrentPageNumber();
            $limit = $paginateData->getItemNumberPerPage();

            $resumeList = [];

            foreach($dataItem as $item) {

                $resume = $resumeService->getIndividualResume($item['user_id'], $jobId);
                unset($item['user_id']);
                $resume['total_experice'] = $item;
                $resumeList[] = $resume;
            }
            $job = $resumeService->getJobDetails($jobId);

            return $this->handleView($this->view(['status'=>'ok', 'data'=> $resumeList,'page'=>$pageNumber,'limit'=>$limit, 'total'=>$totalCount, 'job'=>$job ],Response::HTTP_OK));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/{candidateId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/{jobId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/pdf", name="do_pdf", methods={"GET"})
     * @param Request $request
     * @param string $candidateId
     * @param string $jobId
     * @param DoPdfService $doPdfService
     * @return Response
     */
    public function pdfAction(Request $request, string $candidateId, string $jobId, DoPdfService $doPdfService):Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1 ) {
            //$userId = $request->headers->get('ja-user-id');
            //$userId = 1;
            $returnTemplateTypeData  = self::_decision($candidateId);
            if($returnTemplateTypeData['error']!=''){
                return $this->handleView($this->view(['status'=>'error', 'message'=>$returnTemplateTypeData['error']],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            // templateType =1 =default , 2 = silver type , 3= gold type, 4 = platinum
            if($returnTemplateTypeData['templateType'] == '2' && $returnTemplateTypeData['subscriptionId']!=""){
                return $this->redirectToRoute('resume_silver_index', ['userId'=>$candidateId]);
            }
            $data=[];
            $em = $this->getDoctrine()->getManager();
            $existingBiodata = $em->getRepository('App:Biodata')->findOneBy(['userId'=>$candidateId, 'status'=>1]);
            $data['biodata'] = $existingBiodata;
            $existingQualification = $em->getRepository('App:Qualification')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['qualification'] = $existingQualification;
            $existingExperiences = $em->getRepository('App:Experiences')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['experiences'] = $existingExperiences;
            $existingCertificates = $em->getRepository('App:Certificates')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['certificates'] = $existingCertificates;
            $existingAwards = $em->getRepository('App:Awards')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['awards'] = $existingAwards;
            $existingAdditionalGroup = $em->getRepository('App:AdditionalGroup')->findBy(['userId'=>$candidateId, 'status'=>1]);
            $data['additionalGroup'] = $existingAdditionalGroup;
            $existingApplicationInfo = $em->getRepository('App:ApplicationInfo')->findOneBy(['userId'=>$candidateId, 'status'=>1]);
            $data['applicationInfo'] = $existingApplicationInfo;
            $existingSkills = $em->getRepository('App:Skills')->getSkillListByUser($candidateId);
            $data['skills'] = $existingSkills;
            $userTotalExperiences = $em->getRepository('App:Biodata')->calculateTotalExperiences($candidateId);
            $data['userTotalExperiences'] = $userTotalExperiences;
            $userSalaryExpectation = $em->getRepository('App:JobAppliers')->findOneBy(['userId'=>$candidateId, 'jobId'=>$jobId, 'status'=>1]);
            $data['userSalaryExpectation'] = $userSalaryExpectation;
            //return $this->handleView($this->view(['status'=>'ok','file'=>$data],Response::HTTP_OK));
            try {
                $doPdfService->_pdfAction($candidateId, $data, self::TEMPLATE_TYPE);
               // return $this->handleView($this->view(['status'=>'ok','file'=>$tt],Response::HTTP_OK));
                // in House
                // Need Absolute path
                $absPath = $this->generateUrl('resume_index',[],UrlGeneratorInterface::ABSOLUTE_URL);
                $absPath = str_ireplace('index.php/resume/','',$absPath);
                $file = $absPath.'resume_pdf/'.$candidateId.'/'.$data['biodata']->getFullName().'.pdf';
                return $this->handleView($this->view(['status'=>'ok','file'=>$file, 'name'=>$data['biodata']->getFullName()],Response::HTTP_OK));
            } catch (\Exception $e) {
                return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/applicant/{applicantId<([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}>}/{jobId}", defaults={"jobId" = null}, name="get_pdf_url", methods={"GET"})
     * @param Request $request
     * @param string $applicantId
     * @param string $jobId
     * @param DoPdfService $doPdfService
     * @return Response
     */
    public function getPdfUrl(Request $request, string $applicantId, DoPdfService $doPdfService, string $jobId = null):Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1 ) {
            //$userId = $request->headers->get('ja-user-id');
            //$userId = 1;
            $returnTemplateTypeData  = self::_decision($applicantId);
            if($returnTemplateTypeData['error']!=''){
                return $this->handleView($this->view(['status'=>'error', 'message'=>$returnTemplateTypeData['error']],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            // templateType =1 =default , 2 = silver type , 3= gold type, 4 = platinum
            if($returnTemplateTypeData['templateType'] == '2' && $returnTemplateTypeData['subscriptionId']!=""){
                return $this->redirectToRoute('resume_silver_index', ['userId'=>$applicantId]);
            }
            $data=[];
            $em = $this->getDoctrine()->getManager();
            $existingBiodata = $em->getRepository('App:Biodata')->findOneBy(['userId'=>$applicantId, 'status'=>1]);
            $data['biodata'] = $existingBiodata;
            $existingQualification = $em->getRepository('App:Qualification')->findBy(['userId'=>$applicantId, 'status'=>1]);
            $data['qualification'] = $existingQualification;
            $existingExperiences = $em->getRepository('App:Experiences')->findBy(['userId'=>$applicantId, 'status'=>1]);
            $data['experiences'] = $existingExperiences;
            $existingApplicationInfo = $em->getRepository('App:ApplicationInfo')->findOneBy(['userId'=>$applicantId, 'status'=>1]);
            $data['applicationInfo'] = $existingApplicationInfo;
            $existingCertificates = $em->getRepository('App:Certificates')->findBy(['userId'=>$applicantId, 'status'=>1]);
            $data['certificates'] = $existingCertificates;
            $existingAwards = $em->getRepository('App:Awards')->findBy(['userId'=>$applicantId, 'status'=>1]);
            $data['awards'] = $existingAwards;
            $existingAdditionalGroup = $em->getRepository('App:AdditionalGroup')->findBy(['userId'=>$applicantId, 'status'=>1]);
            $data['additionalGroup'] = $existingAdditionalGroup;
            $existingSkills = $em->getRepository('App:Skills')->getSkillListByUser($applicantId);
            $data['skills'] = $existingSkills;
            $userTotalExperiences = $em->getRepository('App:Biodata')->calculateTotalExperiences($applicantId);
            $data['userTotalExperiences'] = $userTotalExperiences;
            if($jobId!='') {
                $userSalaryExpectation = $em->getRepository('App:JobAppliers')->findOneBy(['userId' => $applicantId, 'jobId' => $jobId, 'status' => 1]);
                $data['userSalaryExpectation'] = $userSalaryExpectation;
            }
            //return $this->handleView($this->view(['status'=>'ok','file'=>$data],Response::HTTP_OK));
            try {
                $doPdfService->_pdfAction($applicantId, $data, self::TEMPLATE_TYPE);
                // return $this->handleView($this->view(['status'=>'ok','file'=>$tt],Response::HTTP_OK));
                // in House
                // Need Absolute path
                $file = 'resume_pdf/'.$applicantId.'/'.$data['biodata']->getFullName().'.pdf';
                return $this->handleView($this->view(['status'=>'ok','file'=>$file, 'name'=>$data['biodata']->getFullName()],Response::HTTP_OK));
            } catch (\Exception $e) {
                return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/biodata", name="modify_biodata", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function ajaxPostBioData(Request $request,  ValidatorInterface $validator): Response
    {
        //dump($request->getContent());
        //die();
        //return $this->json(['status'=>'Ok']);
        // echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
//            return $this->handleView($this->view($contents) );
//            die();

            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId = 1;
            $existingBiodata = $em->getRepository('App:Biodata')->findOneBy(['userId'=>$userId]);
            //return $this->json(['status'=>$existingBiodata->getPhoto()]);die();
            //Get Default Template
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>'1', 'templateType'=> self::TEMPLATE_TYPE]);
            $fullName = $contents["full_name"];
            $contactEmail = $contents["contact_email"];
            $address = $contents["address"];
            $city = $contents["city"];
            $zipPostCode = $contents["zip_post_code"];
            $countryId = $contents["country_id"];
            $country = $em->getRepository('App:Countries')->findOneBy(['id'=>$countryId]);
            $nationality = $contents["nationality"];
            $dateOfBirth = $contents["date_of_birth"];
            $gender = $contents["gender"];
            $identityNumber = $contents["identity_number"];
            $mobileNumber = $contents["mobile_number"];
            $anotherMobileNumber = $contents["another_mobile_number"];
            $objectives = $contents["objectives"];
            $photo = $contents["photo"];
            $careerDescription = $contents["career_description"];
            $lenOfObjectives = strlen($objectives);
            if($lenOfObjectives>2000){
                $objectives= substr($objectives,0,2000);
            }
            $lenOfcareerDescription = strlen($careerDescription);
            if($lenOfcareerDescription>2000){
                $careerDescription= substr($careerDescription,0,2000);
            }
            $noticePeriod = $contents["notice_period"];
            try{
                if($existingBiodata == null){
                    // Set BioData
                    $biodata = new Biodata();
                    $biodata->setUserId($userId);
                    $biodata->setFullName($fullName);
                    $biodata->setContactEmail($contactEmail);
                    $biodata->setAddress($address);
                    $biodata->setCity($city);
                    $biodata->setZipPostCode($zipPostCode);
                    $biodata->setCountryId($country);
                    $biodata->setNationality($nationality);
                    $biodata->setDateOfBirth($dateOfBirth);
                    $biodata->setGender($gender);
                    $biodata->setIdentityNumber($identityNumber);
                    $biodata->setMobileNumber($mobileNumber);
                    $biodata->setAnotherNumber($anotherMobileNumber);
                    $biodata->setObjectives($objectives);
                    $biodata->setPhoto($photo);
                    $biodata->setCareerDescription($careerDescription);
                    $biodata->setNoticePeriod($noticePeriod);
                    $biodata->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($biodata);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($biodata);
                }else{
                    $existingBiodata->setFullName($fullName);
                    $existingBiodata->setAddress($address);
                    $existingBiodata->setContactEmail($contactEmail);
                    $existingBiodata->setCity($city);
                    $existingBiodata->setZipPostCode($zipPostCode);
                    $existingBiodata->setCountryId($country);
                    $existingBiodata->setNationality($nationality);
                    $existingBiodata->setDateOfBirth($dateOfBirth);
                    $existingBiodata->setGender($gender);
                    $existingBiodata->setIdentityNumber($identityNumber);
                    $existingBiodata->setMobileNumber($mobileNumber);
                    $existingBiodata->setAnotherNumber($anotherMobileNumber);
                    $existingBiodata->setObjectives($objectives);
                    $existingBiodata->setPhoto($photo);
                    $existingBiodata->setCareerDescription($careerDescription);
                    $existingBiodata->setNoticePeriod($noticePeriod);
                    $errors = $validator->validate($existingBiodata);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $existingBiodata->setUpdateAt(new \DateTime());
                }
                $em->flush();
            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            $getStepStatus = $this->__updateResumeStatus('1', $template->getId(), $userId);
            if($getStepStatus == false){
                return $this->handleView($this->view(['status'=>'error', 'message'=>'Resume Step not Found'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/experiences", name="modify_experiences_info", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function ajaxPostExperiencesAndInfo(Request $request,  ValidatorInterface $validator): Response
    {
        //return $this->json(['status'=>'Ok']);
        //echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
//            die();
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId=1;
            //Get Default Template
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>'1', 'templateType'=> self::TEMPLATE_TYPE]);
            $experiencesArray = isset($contents['experiences'])?$contents['experiences']:[];
            $applicationInfoArray = isset($contents['applicationInfo'])?$contents['applicationInfo']:[];
            $skillsArray = isset($contents['skills'])?$contents['skills']:[];
            // Operation for experiences
            if(count($experiencesArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'experiences');
                foreach ($experiencesArray as $experiencesData){
                            $experiences = new Experiences();
                            $experiences->setUserId($userId);
                            $experiences->setJobTitle($experiencesData['job_title']);
                            $experiences->setJobCategory(isset($experiencesData['job_category'])?$experiencesData['job_category']:'');
                            $experiences->setJobCategoryTitle(isset($experiencesData['job_category_title'])?$experiencesData['job_category_title']:'');
                            $experiences->setFromDate($experiencesData['from_date']);
                            $experiences->setToDate($experiencesData['to_date']);
                            $experiences->setCompanyName($experiencesData['company_name']);
                            $experiences->setCompanyLocation($experiencesData['company_location']);
                            $experiences->setJobDescription($experiencesData['job_description']);
                            $experiences->setCreatedAt(new \DateTime());
                            $errors = $validator->validate($experiences);
                            if(count($errors) > 0){
                                return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                            }
                            $em->persist($experiences);
                        $em->flush();
                    }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for ApplicationInfo
            if(count($applicationInfoArray)>0){
                    try{
                            $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'application_info');
                            $applicationInfo = new ApplicationInfo();
                            $applicationInfo->setUserId($userId);
                            $applicationInfo->setJobLevel($applicationInfoArray['job_level']);
                            $applicationInfo->setJobCategoryId($applicationInfoArray['job_category_id']);
                            $applicationInfo->setJobCategoryTitle(isset($applicationInfoArray['job_category_title'])?$applicationInfoArray['job_category_title']:'');
                            $applicationInfo->setAvailableFor($applicationInfoArray['available_for']);
                            $applicationInfo->setCreatedAt(new \DateTime());
                            //return $this->handleView($this->view(['status'=>'error','message'=>$applicationInfo],Response::HTTP_INTERNAL_SERVER_ERROR));
                            $errors = $validator->validate($applicationInfo);
                            if(count($errors) > 0){
                                return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                            }
                            $em->persist($applicationInfo);
                        $em->flush();
                    }catch (\Exception $e){
                        return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
            }
            // Operation for Skills
            if(count($skillsArray)>0){
                    try{
                            $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'skills');
                            $skills = new Skills();
                            $skills->setUserId($userId);
                            $skills->setSkillId($skillsArray['skill_id']);
                            $skills->setSkillTitle($skillsArray['skill_title']);
                            $skills->setCreatedAt(new \DateTime());
                            $errors = $validator->validate($skills);
                            if(count($errors) > 0){
                                return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                            }
                            $em->persist($skills);
                        $em->flush();
                    }catch (\Exception $e){
                        return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
            }
            $getStepStatus = $this->__updateResumeStatus('2', $template->getId(), $userId);
            if($getStepStatus == false){
                return $this->handleView($this->view(['status'=>'error','message'=>'Resume step not defined'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/qualification", name="modify_qualification_certificate", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function ajaxPostQualificationAndCertificate(Request $request,  ValidatorInterface $validator): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId = 1;
            //Get Default Template
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>'1', 'templateType'=> self::TEMPLATE_TYPE]);
            $qualificationArray = isset($contents['qualification'])?$contents['qualification']:[];
            // Operation for Qualification
            if(count($qualificationArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'qualification');
                foreach ($qualificationArray as $qualificationData){
                    $examId = $qualificationData['exam_id'];
                    $exam = $em->getRepository('App:ExamList')->findOneBy(['id'=>$examId]);
                    $qualification = new Qualification();
                    $qualification->setUserId($userId);
                    $qualification->setExam($exam);
                    $qualification->setSubject($qualificationData['subject']);
                    $qualification->setInstitute($qualificationData['institute']);
                    $qualification->setResult($qualificationData['result']);
                    $qualification->setStartYear($qualificationData['start_year']);
                    $qualification->setEndYear($qualificationData['end_year']);
                    $qualification->setDetails($qualificationData['details']);
                    $qualification->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($qualification);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($qualification);
                }
                    $em->flush();
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }

            $getStepStatus = $this->__updateResumeStatus('3', $template->getId(), $userId);
            if($getStepStatus == false){
                return $this->handleView($this->view(['status'=>'error', 'message'=>'Resume step not found'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/awards", name="modify_awards_info", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function ajaxPostAwardsAndOthersInfo(Request $request,  ValidatorInterface $validator): Response
    {
        //return $this->json(['status'=>'Ok']);
        //echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //Get Default Template
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>'1', 'templateType'=> self::TEMPLATE_TYPE]);
            $awardsArray = isset($contents['awards'])?$contents['awards']:[];
            $additionalGroupArray = isset($contents['additionalGroup'])?$contents['additionalGroup']:[];
            $certificateArray = isset($contents['certificates'])?$contents['certificates']:[];
            // Operation for Certificates
            if(count($certificateArray)>0){
                try{
                    $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'certificates');
                    foreach ($certificateArray as $certificateData){
                        $certificates = new Certificates();
                        $certificates->setUserId($userId);
                        $certificates->setCertificate($certificateData['certificate']);
                        $certificates->setCertificateDetails($certificateData['certificate_details']);
                        $certificates->setValidity($certificateData['validity']);
                        $certificates->setInstitution($certificateData['institution']);
                        $certificates->setCertificateOrLicenceNumber($certificateData['certificate_or_licence_number']);
                        $certificates->setCreatedAt(new \DateTime());
                        $errors = $validator->validate($certificates);
                        if(count($errors) > 0){
                            return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                        }
                        $em->persist($certificates);
                        $em->flush();
                    }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for awards
            if(count($awardsArray)>0){
                try{
                    $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'awards');
                    foreach ($awardsArray as $awardsData){
                        $awards = new Awards();
                        $awards->setUserId($userId);
                        $awards->setAwardTitle($awardsData['award_title']);
                        $awards->setAwardDetails($awardsData['award_details']);
                        $awards->setAwardDate($awardsData['award_date']);
                        $awards->setCreatedAt(new \DateTime());
                        $errors = $validator->validate($awards);
                        if(count($errors) > 0){
                            return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                        }
                        $em->persist($awards);
                        $em->flush();
                    }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for $additionalGroupArray
            if(count($additionalGroupArray)>0){
                try{
                    $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'additional_group');
                    foreach ($additionalGroupArray as $additionalGroupData){
                        $additionalGroup = new AdditionalGroup();
                        $additionalGroup->setUserId($userId);
                        $additionalGroup->setGroupTitle($additionalGroupData['group_title']);
                        $additionalGroup->setAdditionalGroupDescription($additionalGroupData['additional_group_description']);
                        $additionalGroup->setFromDate($additionalGroupData['from_date']);
                        $additionalGroup->setEndDate($additionalGroupData['end_date']);
                        $additionalGroup->setCreatedAt(new \DateTime());
                        $errors = $validator->validate($additionalGroup);
                        if(count($errors) > 0){
                            return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                        }
                        $em->persist($additionalGroup);
                        $em->flush();
                    }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            $resumeStatus = 2;
            $getStepStatus = $this->__updateResumeStatus('4', $template->getId(), $userId, $resumeStatus);
            if($getStepStatus == false){
                return $this->handleView($this->view(['status'=>'error', 'message'=>'Resume step not found'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

}
