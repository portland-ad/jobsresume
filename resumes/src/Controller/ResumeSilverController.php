<?php

namespace App\Controller;

use App\Controller\traits\Subscriber;
use App\Controller\traits\UpdateResumeStatus;
use App\Entity\AdditionalGroup;
use App\Entity\ApplicationInfo;
use App\Entity\Awards;
use App\Entity\Biodata;
use App\Entity\Certificates;
use App\Entity\Experiences;
use App\Entity\MilitaryServices;
use App\Entity\Qualification;
use App\Entity\Skills;
use App\Entity\Training;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * @Route("/resume/silver")
 */
class ResumeSilverController extends AbstractFOSRestController
{
    use Subscriber;
    use UpdateResumeStatus;
    CONST TEMPLATE_TYPE = 2;
    
    /**
     * @Route("/{userId}", name="resume_silver_index")
     * @param string $userId
     * @return Response
     */
    public function index( string $userId ): Response
    {
        if($this->isSubscriber($userId, self:: TEMPLATE_TYPE )){
            $em = $this->getDoctrine()->getManager();
            $data=[];
            $existingBiodata = $em->getRepository('App:Biodata')->findOneBy(['userId'=>$userId, 'status'=>1]);
            $data['biodata'] = $existingBiodata;
            $existingQualification = $em->getRepository('App:Qualification')->findBy(['userId'=>$userId, 'status'=>1],['startYear'=>'DESC']);
            $data['qualification'] = $existingQualification;
            $existingCertificates = $em->getRepository('App:Certificates')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['certificates'] = $existingCertificates;
            $existingTraining = $em->getRepository('App:Training')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['training'] = $existingTraining;
            $existingExperiences = $em->getRepository('App:Experiences')->findBy(['userId'=>$userId, 'status'=>1],['fromDate'=>'DESC']);
            $data['experiences'] = $existingExperiences;
            $existingExperiences = $em->getRepository('App:Experiences')->findBy(['userId'=>$userId, 'status'=>1],['fromDate'=>'DESC']);
            $data['experiences'] = $existingExperiences;
            $userTotalExperiences = $em->getRepository('App:Biodata')->calculateTotalExperiences($userId);
            $data['userTotalExperiences'] = $userTotalExperiences;
            $existingApplicationInfo = $em->getRepository('App:ApplicationInfo')->findOneBy(['userId'=>$userId, 'status'=>1]);
            $data['applicationInfo'] = $existingApplicationInfo;
            $existingSkills = $em->getRepository('App:Skills')->getSkillListByUser($userId);
            $data['skills'] = $existingSkills;
            $existingAwards = $em->getRepository('App:Awards')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['awards'] = $existingAwards;
            $existingMilitaryServices = $em->getRepository('App:MilitaryServices')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['militaryServices'] = $existingMilitaryServices;
            $existingAdditionalGroup = $em->getRepository('App:AdditionalGroup')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['additionalGroup'] = $existingAdditionalGroup;
            $existingTemplate = $em->getRepository('App:EmployeeTemplate')->findBy(['userId'=>$userId, 'status'=>1]);
            $data['userTemplate'] = $existingTemplate;
            $existingExamList = $em->getRepository('App:ExamList')->findAll();
            $data['examList'] = $existingExamList;
            $existingSkillList = $em->getRepository('App:SkillLists')->findAll();
            $data['skillList'] = $existingSkillList;
            $existingCountryList = $em->getRepository('App:Countries')->findAll();
            $data['countryList'] = $existingCountryList;
            return $this->handleView($this->view(['status'=>'ok','data'=>$data],Response::HTTP_ACCEPTED));
            //return $this->handleView($this->view(['status'=>'ok','data'=>'Ok'],Response::HTTP_ACCEPTED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'UnSubscribe User '],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }
    
    /**
     * @Route("/{userId}/biodata", name="modify_silver_biodata", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function ajaxPostBioData(Request $request,  ValidatorInterface $validator): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId = 1;
            if($this->isSubscriber($userId, self:: TEMPLATE_TYPE ) == false){
                return $this->handleView($this->view(['status'=>'error' ,'message'=>'UnSubscribe User '],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            $existingBiodata = $em->getRepository('App:Biodata')->findOneBy(['userId'=>$userId]);
            //return $this->json(['status'=>$existingBiodata->getPhoto()]);die();
            //Get Default Template
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>'1', 'templateType'=> self::TEMPLATE_TYPE]);
            $fullName = $contents["full_name"];
            $contactEmail = $contents["contact_email"];
            $coverLetter = $contents["cover_letter"];
            $address = $contents["address"];
            $city = $contents["city"];
            $zipPostCode = $contents["zip_post_code"];
            $countryId = $contents["country_id"];
            $country = $em->getRepository('App:Countries')->findOneBy(['id'=>$countryId]);
            $nationality = $contents["nationality"];
            $dateOfBirth = $contents["date_of_birth"];
            $gender = $contents["gender"];
            $identityNumber = $contents["identity_number"];
            $mobileNumber = $contents["mobile_number"];
            $anotherMobileNumber = $contents["another_mobile_number"];
            $objectives = $contents["objectives"];
            $photo = $contents["photo"];
            $careerDescription = $contents["career_description"];
            $noticePeriod = $contents["notice_period"];
            try{
                if($existingBiodata == null){
                    // Set BioData
                    $biodata = new Biodata();
                    $biodata->setUserId($userId);
                    $biodata->setFullName($fullName);
                    $biodata->setContactEmail($contactEmail);
                    $biodata->setAddress($address);
                    $biodata->setCity($city);
                    $biodata->setZipPostCode($zipPostCode);
                    $biodata->setCountryId($country);
                    $biodata->setNationality($nationality);
                    $biodata->setDateOfBirth($dateOfBirth);
                    $biodata->setGender($gender);
                    $biodata->setIdentityNumber($identityNumber);
                    $biodata->setMobileNumber($mobileNumber);
                    $biodata->setAnotherNumber($anotherMobileNumber);
                    $biodata->setObjectives($objectives);
                    $biodata->setCoverLetter($coverLetter);
                    $biodata->setPhoto($photo);
                    $biodata->setCareerDescription($careerDescription);
                    $biodata->setNoticePeriod($noticePeriod);
                    $biodata->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($biodata);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($biodata);
                }else{
                    $existingBiodata->setFullName($fullName);
                    $existingBiodata->setAddress($address);
                    $existingBiodata->setContactEmail($contactEmail);
                    $existingBiodata->setCity($city);
                    $existingBiodata->setZipPostCode($zipPostCode);
                    $existingBiodata->setCountryId($country);
                    $existingBiodata->setNationality($nationality);
                    $existingBiodata->setDateOfBirth($dateOfBirth);
                    $existingBiodata->setGender($gender);
                    $existingBiodata->setIdentityNumber($identityNumber);
                    $existingBiodata->setMobileNumber($mobileNumber);
                    $existingBiodata->setAnotherNumber($anotherMobileNumber);
                    $existingBiodata->setObjectives($objectives);
                    $existingBiodata->setCoverLetter($coverLetter);
                    $existingBiodata->setPhoto($photo);
                    $existingBiodata->setCareerDescription($careerDescription);
                    $existingBiodata->setNoticePeriod($noticePeriod);
                    $errors = $validator->validate($existingBiodata);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $existingBiodata->setUpdateAt(new \DateTime());
                }
                $em->flush();
            }catch (\Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            $getStepStatus = $this->__updateResumeStatus('1', $template->getId(), $userId);
            if($getStepStatus == false){
                return $this->handleView($this->view(['status'=>'error', 'message'=>'Resume Step not Found'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/{userId}/experiences", name="modify_silver_experiences_info", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function ajaxPostExperiencesAndInfo(Request $request,  ValidatorInterface $validator): Response
    {
        //return $this->json(['status'=>'Ok']);
        //echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
//            die();
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId=1;
            //Get Default Template
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>'1', 'templateType'=> self::TEMPLATE_TYPE]);
            $experiencesArray = $contents['experiences'];
            $applicationInfoArray = $contents['applicationInfo'];
            $skillsArray =  $contents['skills'];
            // Operation for experiences
            if(count($experiencesArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'experiences');
                foreach ($experiencesArray as $experiencesData){
                        $experiences = new Experiences();
                        $experiences->setUserId($userId);
                        $experiences->setJobTitle($experiencesData['job_title']);
                        $experiences->setJobCategory($experiencesData['job_category']);
                        $experiences->setJobCategoryTitle(isset($experiencesData['job_category_title'])?$experiencesData['job_category_title']:'');
                        $experiences->setFromDate($experiencesData['from_date']);
                        $experiences->setToDate($experiencesData['to_date']);
                        $experiences->setCompanyName($experiencesData['company_name']);
                        $experiences->setCompanyLocation($experiencesData['company_location']);
                        $experiences->setJobDescription($experiencesData['job_description']);
                        $experiences->setCreatedAt(new \DateTime());
                        $errors = $validator->validate($experiences);
                        if(count($errors) > 0){
                            return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                        }
                        $em->persist($experiences);
                        $em->flush();
                    }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for ApplicationInfo
            if(count($applicationInfoArray)>0){
                try{
                    $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'application_info');
                    $applicationInfo = new ApplicationInfo();
                    $applicationInfo->setUserId($userId);
                    $applicationInfo->setJobLevel($applicationInfoArray['job_level']);
                    $applicationInfo->setJobCategoryId($applicationInfoArray['job_category_id']);
                    $applicationInfo->setJobCategoryTitle(isset($applicationInfoArray['job_category_title'])?$applicationInfoArray['job_category_title']:'');
                    $applicationInfo->setAvailableFor($applicationInfoArray['available_for']);
                    $applicationInfo->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($applicationInfo);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($applicationInfo);
                    $em->flush();
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for Skills
            if(count($skillsArray)>0){
                try{
                    $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'skills');
                    $skills = new Skills();
                    $skills->setUserId($userId);
                    $skills->setSkillId($skillsArray['skill_id']);
                    $skills->setSkillTitle($skillsArray['skill_title']);
                    $skills->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($skills);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($skills);
                    $em->flush();
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            $getStepStatus = $this->__updateResumeStatus('2', $template->getId(), $userId);
            if($getStepStatus == false){
                return $this->handleView($this->view(['status'=>'error','message'=>'Resume step not defined'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/{userId}/qualification", name="modify_silver_qualification_certificate", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function ajaxPostQualificationAndCertificate(Request $request,  ValidatorInterface $validator): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId = 1;
            if($this->isSubscriber($userId, self:: TEMPLATE_TYPE ) == false){
                return $this->handleView($this->view(['status'=>'error' ,'message'=>'UnSubscribe User '],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            //Get Default Template
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>'1', 'templateType'=> self::TEMPLATE_TYPE]);
            $qualificationArray = $contents['qualification'];
            $certificateArray = isset($contents['certificates'])?$contents['certificates']:[];
            $trainingArray =  isset($contents['training'])?$contents['training']:[];
            // Operation for Qualification
            if(count($qualificationArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'qualification');
                foreach ($qualificationArray as $qualificationData){
                    $examId = $qualificationData['examId'];
                    $exam = $em->getRepository('App:ExamList')->findOneBy(['id'=>$examId]);
                        $qualification = new Qualification();
                        $qualification->setUserId($userId);
                        $qualification->setExam($exam);
                        $qualification->setSubject($qualificationData['subject']);
                        $qualification->setInstitute($qualificationData['institute']);
                        $qualification->setResult($qualificationData['result']);
                        $qualification->setStartYear($qualificationData['start_year']);
                        $qualification->setEndYear($qualificationData['end_year']);
                        $qualification->setDetails($qualificationData['details']);
                        $qualification->setCreatedAt(new \DateTime());
                        $errors = $validator->validate($qualification);
                        if(count($errors) > 0){
                            return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                        }
                        $em->persist($qualification);
                        $em->flush();
                    }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for Certificates
            if(count($certificateArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'certificates');
                foreach ($certificateArray as $certificateData){
                    $certificates = new Certificates();
                    $certificates->setUserId($userId);
                    $certificates->setCertificate($certificateData['certificate']);
                    $certificates->setCertificateDetails($certificateData['certificate_details']);
                    $certificates->setValidity($certificateData['validity']);
                    $certificates->setInstitution($certificateData['institution']);
                    $certificates->setCertificateOrLicenceNumber($certificateData['certificate_or_licenceNumber']);
                    $certificates->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($certificates);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($certificates);
                    $em->flush();
                }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for Training
            if(count($trainingArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'training');
                foreach ($trainingArray as $trainingData){
                    $training = new Training();
                    $training->setUserId($userId);
                    $training->setTitle($trainingData['title']);
                    $training->setTrainingDetails($trainingData['training_details']);
                    $training->setTrainingProvider($trainingData['training_provider']);
                    $training->setPlace($trainingData['place']);
                    $training->setDuration($trainingData['duration']);
                    $training->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($training);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($training);
                    $em->flush();
                }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            $getStepStatus = $this->__updateResumeStatus('3', $template->getId(), $userId);
            if($getStepStatus == false){
                return $this->handleView($this->view(['status'=>'error', 'message'=>'Resume step not found'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }

    /**
     * @Route("/{userId}/awards", name="modify_silver_awards_info", methods={"GET","POST","PUT"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function ajaxPostAwardsAndOthersInfo(Request $request,  ValidatorInterface $validator): Response
    {
        //return $this->json(['status'=>'Ok']);
        //echo 'here';die();
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//            ]);
//            die();
            $em = $this->getDoctrine()->getManager();
            $userId = $request->headers->get('ja-user-id');
            //$userId = 1;
            if($this->isSubscriber($userId, self:: TEMPLATE_TYPE ) == false){
                return $this->handleView($this->view(['status'=>'error' ,'message'=>'UnSubscribe User '],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            //Get Default Template
            $template = $em->getRepository('App:Templates')->findOneBy(['status'=>'1', 'templateType'=> self::TEMPLATE_TYPE]);
            $awardsArray = $contents['awards'];
            $militaryServicesArray = $contents['militaryServices'];
            $additionalGroupArray =  $contents['additionalGroup'];
            // Operation for awards
            if(count($awardsArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'awards');
                foreach ($awardsArray as $awardsData){
                        $awards = new Awards();
                        $awards->setUserId($userId);
                        $awards->setAwardTitle($awardsData['award_title']);
                        $awards->setAwardDetails($awardsData['award_details']);
                        $awards->setAwardDate($awardsData['award_date']);
                        $awards->setCreatedAt(new \DateTime());
                        $errors = $validator->validate($awards);
                        if(count($errors) > 0){
                            return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                        }
                        $em->persist($awards);
                        $em->flush();
                }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for MilitaryServices
            if(count($militaryServicesArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'military_services');
                foreach ($militaryServicesArray as $militaryServicesData){

                    $countryId = $militaryServicesData["countryId"];
                    $country = $em->getRepository('App:Countries')->findOneBy(['id'=>$countryId]);
                    $militaryServices = new MilitaryServices();
                    $militaryServices->setUserId($userId);
                    $militaryServices->setCountryId($country);
                    $militaryServices->setMilitaryBranch($militaryServicesData['military_branch']);
                    $militaryServices->setMilitaryRank($militaryServicesData['military_rank']);
                    $militaryServices->setFromDate($militaryServicesData['from_date']);
                    $militaryServices->setEndDate($militaryServicesData['end_date']);
                    $militaryServices->setMilitaryDescription($militaryServicesData['military_description']);
                    $militaryServices->setCommendations($militaryServicesData['commendations']);
                    $militaryServices->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($militaryServices);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($militaryServices);
                    $em->flush();
                }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            // Operation for $additionalGroupArray
            if(count($additionalGroupArray)>0){
                try{
                $em->getRepository('App:Biodata')->deleteDataByUser($userId, 'additional_group');
                foreach ($additionalGroupArray as $additionalGroupData){
                    $additionalGroup = new AdditionalGroup();
                    $additionalGroup->setUserId($userId);
                    $additionalGroup->setGroupTitle($additionalGroupData['group_title']);
                    $additionalGroup->setAdditionalGroupDescription($additionalGroupData['additional_group_description']);
                    $additionalGroup->setFromDate($additionalGroupData['from_date']);
                    $additionalGroup->setEndDate($additionalGroupData['end_date']);
                    $additionalGroup->setCreatedAt(new \DateTime());
                    $errors = $validator->validate($additionalGroup);
                    if(count($errors) > 0){
                        return $this->handleView($this->view(['status'=>'error', 'message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
                    }
                    $em->persist($additionalGroup);
                    $em->flush();
                 }
                }catch (\Exception $e){
                    return $this->handleView($this->view(['status'=>'error', 'message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
                }
            }
            $resumeStatus = 2;
            $getStepStatus = $this->__updateResumeStatus('4', $template->getId(), $userId, $resumeStatus);
            if($getStepStatus == false){
                return $this->handleView($this->view(['status'=>'error', 'message'=>'Resume step not found'],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error' ,'message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
    }
}