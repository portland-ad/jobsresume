<?php


namespace App\Controller\traits;


use Symfony\Component\HttpClient\HttpClient;

trait userToken
{
    protected function __getUserToken($gatewayAdminUrl, $gatewayAdminApiKey, $consumerId )
    {
       // dump($client);die();
        $tokens = null ;
        $client = HttpClient::create();
        if ($consumerId) {
            $gatewayData = $client->request('GET', $gatewayAdminUrl . '/tokens/' . $consumerId, [
                'headers' => [
                    'Authorization' => 'apikey ' . $gatewayAdminApiKey
                ]
            ]);

            $status = $gatewayData->getStatusCode();

            if ($status != 200) {
                return '';
            }
            return $gatewayData->toArray();
        }
    }

}