<?php
    
    namespace App\Controller\traits;

    trait Subscriber
    {
        protected function isSubscriber(string $userId, string $templateType){
            $em = $this->getDoctrine()->getManager();
            $existingTemplate = $em->getRepository('App:Templates')->findOneBy(['templateType'=>$templateType, 'status'=>1]);
            $existingData = $em->getRepository('App:EmployeeTemplate')->findOneBy(['userId'=>$userId, 'templateID'=> $existingTemplate, 'status'=>1]);
            if($existingData != null && $existingData->getSubscriptionId()!="" ){
                return true;
            }
            return false;
        }
    }