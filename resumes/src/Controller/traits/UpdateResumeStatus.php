<?php
    
    
    namespace App\Controller\traits;
    
    use App\Entity\ResumeStatus;

    trait UpdateResumeStatus
    {
        protected function __updateResumeStatus($step, $templateId, $userId, $status =1){
            // $status = 1 = incomplete, 2= complete
            $em = $this->getDoctrine()->getManager();
            $existingResumeStatus = $em->getRepository('App:ResumeStatus')->findOneBy(['userId'=>$userId]);
            //Set Employee-Template
            try {
                if($existingResumeStatus == null){
                    $resumeStatus = new ResumeStatus();
                    $resumeStatus->setUserId($userId);
                    $resumeStatus->setTemplateId($templateId);
                    $resumeStatus->setStep($step);
                    $resumeStatus->setStatus($status);
                    $resumeStatus->setCreatedAt(new \DateTime());
                    $em->persist($resumeStatus);
                }else{
                    $existingResumeStatus->setUserId($userId);
                    $existingResumeStatus->setTemplateId($templateId);
                    $existingResumeStatus->setStep($step);
                    $existingResumeStatus->setStatus($status);
                    $existingResumeStatus->setUpdateAt(new \DateTime());
                }
                $em->flush();
                return true;
            }catch (\Exception $e){
                return false;
            }
        }
        
    }