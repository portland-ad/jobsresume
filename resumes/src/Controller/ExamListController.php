<?php

namespace App\Controller;

use App\Entity\ExamList;
use App\Form\ExamListType;
use App\Repository\ExamListRepository;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/resume/exam/list")
 */
class ExamListController extends AbstractFOSRestController
{
    /**
     * @Route("/", name="exam_list_index", methods={"GET"})
     * @param Request $request
     * @param ExamListRepository $examListRepository
     * @return Response
     */
    public function index(Request $request, ExamListRepository $examListRepository): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            return$this->handleView($this->view($examListRepository->findAll()));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }

    /**
     * @Route("/new", name="exam_list_new", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     * @throws Exception
     */
    public function new(Request $request, ValidatorInterface $validator): Response
    {
        if(strpos($request->headers->get('content-type'),"application/json") > -1){
            $contents = json_decode($request->getContent(),true);
//            return $this->json(['consumerId'=>$request->headers->get('ja-consumer-id'),
//                'mysqlUserId'=>$request->headers->get('ja-user-id'),
//                'UserType'=>$request->headers->get('ja-user-type')
//                ]);
            //return $this->handleView($this->view($contents) );
            // die();
            $examList = new ExamList();
            $examList->setTitle($contents["examTitle"]);
            $examList->setCreatedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($examList);
            //$entityManager->flush();
            $errors = $validator->validate($examList);
            if(count($errors) > 0){
                return $this->handleView($this->view(['status'=>'error','message'=>$errors],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            try{
                $entityManager->flush();
            }catch (Exception $e){
                return $this->handleView($this->view(['status'=>'error','message'=>$e],Response::HTTP_INTERNAL_SERVER_ERROR));
            }
            return $this->handleView($this->view(['status'=>'ok'],Response::HTTP_CREATED));
        }
        return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
    }

    /**
     * @Route("/{id}", name="exam_list_show", methods={"GET"})
     */
    public function show(ExamList $examList): Response
    {
        return $this->render('exam_list/show.html.twig', [
            'exam_list' => $examList,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="exam_list_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ExamList $examList): Response
    {
        $form = $this->createForm(ExamListType::class, $examList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('exam_list_index');
        }

        return $this->render('exam_list/edit.html.twig', [
            'exam_list' => $examList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="exam_list_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ExamList $examList): Response
    {
        if ($this->isCsrfTokenValid('delete'.$examList->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($examList);
            $entityManager->flush();
        }

        return $this->redirectToRoute('exam_list_index');
    }
}
