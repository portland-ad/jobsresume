<?php

namespace App\Controller\Admin;

use App\Entity\ExamList;
use App\Entity\RuleDetails;
use App\Entity\Templates;
use App\Form\TemplatesType;
use App\Repository\TemplatesRepository;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
//use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;



class AdminTemplateController extends AbstractFOSRestController
{
    /**
     * @Route("/admin/resume/template", methods={"GET"})
     * @param Request $request
     * @param TemplatesRepository $templatesRepository
     * @return Response
     */
    public function getAllTemplate(Request $request, TemplatesRepository $templatesRepository):Response
    {
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                return $this->handleView($this->view($templatesRepository->findAll(),Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
    }

    /**
     * @Route("/admin/resume/template", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createNewTemplate(Request $request){
        $template = new Templates();
        try{
            $em = $this->getDoctrine()->getManager();
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);

                if(!isset($requestData['template_name'])) {
                    throw new Exception('template_name is missing');
                }
                if(empty($requestData['template_name'])) {
                    throw new Exception('template name can not empty');
                }
                if(!isset($requestData['template_type'])) {
                    throw new Exception('template_type is missing');
                }
                if(empty($requestData['template_type'])) {
                    throw new Exception('template type can not empty');
                }

                $template->setCreatedAt(new \DateTime());
                $template->setStatus(1);
                $template->setUpdateAt(new \DateTime());
                $template->setTemplateType($requestData['template_type']);
                $template->setTemplateName($requestData['template_name']);
                $em->persist($template);
                $em->flush();
                return $this->handleView($this->view(['template'=>$template,'status'=>'ok'],Response::HTTP_CREATED));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],200));
        }
    }

     /**
     * @Route("/admin/resume/template/{id}", methods={"PUT"})
     * @param Request $request
     * @return Response
     */
    public function updateTemplate(Request $request, String $id){
        $em = $this->getDoctrine()->getManager();
        $template =   $em->getRepository(Templates::class)->find($id);
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);
                if(isset($requestData['template_name'])) {
                    $template->setTemplateName($requestData['template_name']);
                }
                if(isset($requestData['template_type'])) {
                    $template->setTemplateType($requestData['template_type']);
                }
                $template->setUpdateAt(new \DateTime());             
                $em->flush();
                return $this->handleView($this->view(['template'=>$template,'status'=>'ok'], 206));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],200));
        }
    }
}
