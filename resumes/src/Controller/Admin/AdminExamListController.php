<?php


namespace App\Controller\Admin;


use App\Entity\ExamList;
use App\Repository\ExamListRepository;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminExamListController extends AbstractFOSRestController
{
 /**
 * @Route("/admin/resume/exams", name="admin_exam_list_index", methods={"GET"})
 * @param Request $request
 * @param ExamListRepository $examListRepository
 * @return Response
 */
    public function index(Request $request, ExamListRepository $examListRepository): Response
    {
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                return $this->handleView($this->view($examListRepository->findAll(),Response::HTTP_OK));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }
    }




    /**
     * @Route("/admin/resume/exams", name="admin_exam_lists_create", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function createNewExam(Request $request):Response
    {


        $exam = new ExamList();
        try{
            $em = $this->getDoctrine()->getManager();
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);
                if(!isset($requestData['title'])) {
                    throw new Exception('Title is missing');
                }
                if(empty($requestData['title'])) {
                    throw new Exception('Title can not be empty');
                }

                $exam->setCreatedAt(new \DateTime());
                $exam->setStatus(1);
                $exam->setUpdateAt(new \DateTime());
                $exam->setTitle($requestData['title']);
                $em->persist($exam);
                $em->flush();
                return $this->handleView($this->view(['status'=>'ok','exam'=>$exam],Response::HTTP_CREATED));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }

    }

 /**
     * @Route("/admin/resume/exams/{id}", name="admin_exam_lists_update", methods={"PUT"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function UpdateNewExam(Request $request, String $id):Response
    {

        $em = $this->getDoctrine()->getManager();
        $exam = null;
        try{
            $exam = $em->getRepository(ExamList::class)->find($id);
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);
                if(isset($requestData['title'])) {
                    $exam->setTitle($requestData['title']);
                }
                if(isset($requestData['status'])) {
                    $exam->setStatus($requestData['status']);
                }

                $exam->setUpdateAt(new \DateTime());
                $em->flush();
                return $this->handleView($this->view(['status'=>'ok','exam'=>$exam],206));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }

    }


}