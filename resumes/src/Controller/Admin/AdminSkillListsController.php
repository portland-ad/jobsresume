<?php


namespace App\Controller\Admin;


use App\Entity\SkillLists;
use App\Repository\SkillListsRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminSkillListsController extends AbstractFOSRestController
{
    /**
     * @Route("/admin/resume/skills", name="admin_skill_lists_index", methods={"GET"})
     * @param Request $request
     * @param SkillListsRepository $skillListsRepository
     * @return Response
     */
    public function index(Request $request, SkillListsRepository $skillListsRepository): Response
    {

        try {
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                return $this->handleView($this->view($skillListsRepository->findAll()),Response::HTTP_OK);
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_INTERNAL_SERVER_ERROR));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        }

    }


    /**
     * @Route("/admin/resume/skills", name="admin_skill_lists_create", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function createNewSkill(Request $request):Response
    {
        $skill = new SkillLists();
        try{
            $em = $this->getDoctrine()->getManager();
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);

                if(!isset($requestData['title'])) {
                    throw new Exception('Title is missing');
                }

                $skill->setCreatedAt(new \DateTime());
                $skill->setStatus(1);
                $skill->setUpdateAt(new \DateTime());
               $skill->setTitle($requestData['title']);
                $em->persist($skill);
                $em->flush();
                return $this->handleView($this->view(['status'=>'ok','skill_list'=>$skill],Response::HTTP_CREATED));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }

    }


     /**
     * @Route("/admin/resume/skills/{id}", name="admin_skill_lists_update", methods={"PUT"})
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function updateNewSkill(Request $request, String $id):Response
    {
        $em = $this->getDoctrine()->getManager();
        $skill =   $em->getRepository(SkillLists::class)->find($id);
        try{
            if(strpos($request->headers->get('content-type'),"application/json") > -1){
                $requestData = json_decode($request->getContent(), true);
                if(isset($requestData['title'])) {
                    $skill->setTitle($requestData['title']);
                }

                $skill->setUpdateAt(new \DateTime());
                $em->flush();
                return $this->handleView($this->view(['status'=>'ok','skill_list'=>$skill], 206));
            }
            return $this->handleView($this->view(['status'=>'error','message'=>'Header Content-Type Needs application/json'],Response::HTTP_UNSUPPORTED_MEDIA_TYPE));
        } catch (Exception $e)
        {
            return $this->handleView($this->view(['status'=>'error','message'=>$e->getMessage()],Response::HTTP_INTERNAL_SERVER_ERROR));
        }

    }

}