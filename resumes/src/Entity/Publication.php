<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PublicationRepository")
 */
class Publication
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $publicationTitle;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $publicationURL;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $publicationDescription;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $publicationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPublicationTitle(): ?string
    {
        return $this->publicationTitle;
    }

    public function setPublicationTitle(?string $publicationTitle): self
    {
        $this->publicationTitle = $publicationTitle;

        return $this;
    }

    public function getPublicationURL(): ?string
    {
        return $this->publicationURL;
    }

    public function setPublicationURL(?string $publicationURL): self
    {
        $this->publicationURL = $publicationURL;

        return $this;
    }

    public function getPublicationDescription(): ?string
    {
        return $this->publicationDescription;
    }

    public function setPublicationDescription(?string $publicationDescription): self
    {
        $this->publicationDescription = $publicationDescription;

        return $this;
    }

    public function getPublicationDate(): ?string
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(?string $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
