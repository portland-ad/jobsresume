<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonalInfoRepository")
 */
class PersonalInfo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $userId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $fatherName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $MotherName;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $dateOfBirth;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $gander;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $maritalStaus;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $religion;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nationality;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $permanentAddress;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Countries", inversedBy="datetime")
     */
    private $countryID;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $UserId): self
    {
        $this->userId = $UserId;

        return $this;
    }

    public function getFatherName(): ?string
    {
        return $this->fatherName;
    }

    public function setFatherName(?string $fatherName): self
    {
        $this->fatherName = $fatherName;

        return $this;
    }

    public function getMotherName(): ?string
    {
        return $this->MotherName;
    }

    public function setMotherName(?string $MotherName): self
    {
        $this->MotherName = $MotherName;

        return $this;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?string $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getGander(): ?string
    {
        return $this->gander;
    }

    public function setGander(string $gander): self
    {
        $this->gander = $gander;

        return $this;
    }

    public function getMaritalStaus(): ?string
    {
        return $this->maritalStaus;
    }

    public function setMaritalStaus(string $maritalStaus): self
    {
        $this->maritalStaus = $maritalStaus;

        return $this;
    }

    public function getReligion(): ?string
    {
        return $this->religion;
    }

    public function setReligion(?string $religion): self
    {
        $this->religion = $religion;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getPermanentAddress(): ?string
    {
        return $this->permanentAddress;
    }

    public function setPermanentAddress(?string $permanentAddress): self
    {
        $this->permanentAddress = $permanentAddress;

        return $this;
    }

    public function getCountryID(): ?Countries
    {
        return $this->countryID;
    }

    public function setCountryID(?Countries $countryID): self
    {
        $this->countryID = $countryID;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
