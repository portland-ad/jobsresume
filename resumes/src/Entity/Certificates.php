<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CertificatesRepository")
 */
class Certificates
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $userId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $certificate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $certificateDetails;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status =1;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $certificateOrLicenceNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $validity;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $institution;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getCertificate(): ?string
    {
        return $this->certificate;
    }

    public function setCertificate(?string $certificate): self
    {
        $this->certificate = $certificate;

        return $this;
    }

    public function getCertificateDetails(): ?string
    {
        return $this->certificateDetails;
    }

    public function setCertificateDetails(?string $certificateDetails): self
    {
        $this->certificateDetails = $certificateDetails;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCertificateOrLicenceNumber(): ?string
    {
        return $this->certificateOrLicenceNumber;
    }

    public function setCertificateOrLicenceNumber(?string $certificateOrLicenceNumber): self
    {
        $this->certificateOrLicenceNumber = $certificateOrLicenceNumber;

        return $this;
    }

    public function getValidity(): ?string
    {
        return $this->validity;
    }

    public function setValidity(?string $validity): self
    {
        $this->validity = $validity;

        return $this;
    }

    public function getInstitution(): ?string
    {
        return $this->institution;
    }

    public function setInstitution(?string $institution): self
    {
        $this->institution = $institution;

        return $this;
    }
}
