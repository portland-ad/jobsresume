<?php

namespace App\Entity;

use App\Repository\JobAppliersRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JobAppliersRepository::class)
 */
class JobAppliers
{
    /**
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $userId;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $shortListed;

    /**
     * @ORM\Column(type="float")
     */
    private $expectedSalary;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $summaryView;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $detailsView;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $interViewCall;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $subscriptionId;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $currencyCode;


    public function getId(): ?string
    {
        return $this->id;
    }

    public function getJobId(): ?string
    {
        return $this->jobId;
    }

    public function setJobId(string $jobId): self
    {
        $this->jobId = $jobId;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getShortListed(): ?int
    {
        return $this->shortListed;
    }

    public function setShortListed(?int $shortListed): self
    {
        $this->shortListed = $shortListed;

        return $this;
    }

    public function getExpectedSalary(): ?float
    {
        return $this->expectedSalary;
    }

    public function setExpectedSalary(float $expectedSalary): self
    {
        $this->expectedSalary = $expectedSalary;

        return $this;
    }

    public function getSummaryView(): ?int
    {
        return $this->summaryView;
    }

    public function setSummaryView(?int $summaryView): self
    {
        $this->summaryView = $summaryView;

        return $this;
    }

    public function getDetailsView(): ?int
    {
        return $this->detailsView;
    }

    public function setDetailsView(?int $detailsView): self
    {
        $this->detailsView = $detailsView;

        return $this;
    }

    public function getInterViewCall(): ?int
    {
        return $this->interViewCall;
    }

    public function setInterViewCall(?int $interViewCall): self
    {
        $this->interViewCall = $interViewCall;

        return $this;
    }

    public function getSubscriptionId(): ?string
    {
        return $this->subscriptionId;
    }

    public function setSubscriptionId(?string $subscriptionId): self
    {
        $this->subscriptionId = $subscriptionId;

        return $this;
    }

    public function getCurrencyCode(): ?string
    {
        return $this->currencyCode;
    }

    public function setCurrencyCode(string $currencyCode): self
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }
}
