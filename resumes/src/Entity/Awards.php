<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AwardsRepository")
 */
class Awards
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $userId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $awardTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $awardDetails;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $awardDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status =1;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getAwardTitle(): ?string
    {
        return $this->awardTitle;
    }

    public function setAwardTitle(?string $awardTitle): self
    {
        $this->awardTitle = $awardTitle;

        return $this;
    }

    public function getAwardDetails(): ?string
    {
        return $this->awardDetails;
    }

    public function setAwardDetails(?string $awardDetails): self
    {
        $this->awardDetails = $awardDetails;

        return $this;
    }

    public function getAwardDate(): ?string
    {
        return $this->awardDate;
    }

    public function setAwardDate(?string $awardDate): self
    {
        $this->awardDate = $awardDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
