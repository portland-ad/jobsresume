<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApplicationInfoRepository")
 */
class ApplicationInfo
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $userId;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $jobLevel;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $jobCategoryId;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $availableFor;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status = 1;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $jobCategoryTitle;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getJobLevel(): ?string
    {
        return $this->jobLevel;
    }

    public function setJobLevel(?string $jobLevel): self
    {
        $this->jobLevel = $jobLevel;

        return $this;
    }

    public function getJobCategoryId(): ?string
    {
        return $this->jobCategoryId;
    }

    public function setJobCategoryId(?string $jobCategoryId): self
    {
        $this->jobCategoryId = $jobCategoryId;

        return $this;
    }

    public function getAvailableFor(): ?string
    {
        return $this->availableFor;
    }

    public function setAvailableFor(?string $availableFor): self
    {
        $this->availableFor = $availableFor;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getJobCategoryTitle(): ?string
    {
        return $this->jobCategoryTitle;
    }

    public function setJobCategoryTitle(?string $jobCategoryTitle): self
    {
        $this->jobCategoryTitle = $jobCategoryTitle;

        return $this;
    }
}
