<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RuleDetailsRepository")
 */
class RuleDetails
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ruleTitle;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $ruleDescription;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status =1;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getRuleTitle(): ?string
    {
        return $this->ruleTitle;
    }

    public function setRuleTitle(?string $ruleTitle): self
    {
        $this->ruleTitle = $ruleTitle;

        return $this;
    }

    public function getRuleDescription(): ?string
    {
        return $this->ruleDescription;
    }

    public function setRuleDescription(?string $ruleDescription): self
    {
        $this->ruleDescription = $ruleDescription;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }
}
