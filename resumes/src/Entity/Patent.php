<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PatentRepository")
 */
class Patent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $patentNumber;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $userId;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $patentURL;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $patentDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $patentDescription;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPatentNumber(): ?string
    {
        return $this->patentNumber;
    }

    public function setPatentNumber(?string $patentNumber): self
    {
        $this->patentNumber = $patentNumber;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getPatentURL(): ?string
    {
        return $this->patentURL;
    }

    public function setPatentURL(?string $patentURL): self
    {
        $this->patentURL = $patentURL;

        return $this;
    }

    public function getPatentDate(): ?string
    {
        return $this->patentDate;
    }

    public function setPatentDate(?string $patentDate): self
    {
        $this->patentDate = $patentDate;

        return $this;
    }

    public function getPatentDescription(): ?string
    {
        return $this->patentDescription;
    }

    public function setPatentDescription(?string $patentDescription): self
    {
        $this->patentDescription = $patentDescription;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
