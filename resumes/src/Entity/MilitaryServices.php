<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MilitaryServicesRepository")
 */
class MilitaryServices
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid" ,unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Countries")
     */
    private $countryId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $militaryBranch;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $militaryRank;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $fromDate;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $militaryDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commendations;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getCountryId(): ?Countries
    {
        return $this->countryId;
    }

    public function setCountryId(?Countries $countryId): self
    {
        $this->countryId = $countryId;

        return $this;
    }

    public function getMilitaryBranch(): ?string
    {
        return $this->militaryBranch;
    }

    public function setMilitaryBranch(?string $militaryBranch): self
    {
        $this->militaryBranch = $militaryBranch;

        return $this;
    }

    public function getMilitaryRank(): ?string
    {
        return $this->militaryRank;
    }

    public function setMilitaryRank(?string $militaryRank): self
    {
        $this->militaryRank = $militaryRank;

        return $this;
    }

    public function getFromDate(): ?string
    {
        return $this->fromDate;
    }

    public function setFromDate(?string $fromDate): self
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    public function getEndDate(): ?string
    {
        return $this->endDate;
    }

    public function setEndDate(?string $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getMilitaryDescription(): ?string
    {
        return $this->militaryDescription;
    }

    public function setMilitaryDescription(?string $militaryDescription): self
    {
        $this->militaryDescription = $militaryDescription;

        return $this;
    }

    public function getCommendations(): ?string
    {
        return $this->commendations;
    }

    public function setCommendations(?string $commendations): self
    {
        $this->commendations = $commendations;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $UpdateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
